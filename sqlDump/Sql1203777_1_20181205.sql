-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: 89.46.111.63
-- Generato il: Dic 05, 2018 alle 18:24
-- Versione del server: 5.6.41
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sql1203777_1`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_commentmeta`
--

DROP TABLE IF EXISTS `lab_commentmeta`;
CREATE TABLE IF NOT EXISTS `lab_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_comments`
--

DROP TABLE IF EXISTS `lab_comments`;
CREATE TABLE IF NOT EXISTS `lab_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10)),
  KEY `woo_idx_comment_type` (`comment_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `lab_comments`
--

INSERT INTO `lab_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-11-16 15:26:04', '2018-11-16 15:26:04', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_et_bloom_stats`
--

DROP TABLE IF EXISTS `lab_et_bloom_stats`;
CREATE TABLE IF NOT EXISTS `lab_et_bloom_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `record_type` varchar(3) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `optin_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `list_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `page_id` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `removed_flag` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_et_social_stats`
--

DROP TABLE IF EXISTS `lab_et_social_stats`;
CREATE TABLE IF NOT EXISTS `lab_et_social_stats` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `sharing_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `network` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `media_url` varchar(2083) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_links`
--

DROP TABLE IF EXISTS `lab_links`;
CREATE TABLE IF NOT EXISTS `lab_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_actions`
--

DROP TABLE IF EXISTS `lab_nf3_actions`;
CREATE TABLE IF NOT EXISTS `lab_nf3_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext,
  `key` longtext,
  `type` longtext,
  `active` tinyint(1) DEFAULT '1',
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `lab_nf3_actions`
--

INSERT INTO `lab_nf3_actions` (`id`, `title`, `key`, `type`, `active`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, '', '', 'save', 1, 1, '2018-11-25 08:42:23', '2018-11-25 09:42:23'),
(2, '', '', 'email', 1, 1, '2018-11-25 08:42:23', '2018-11-25 09:42:23'),
(3, '', '', 'email', 1, 1, '2018-11-25 08:42:23', '2018-11-25 09:42:23'),
(4, '', '', 'successmessage', 1, 1, '2018-11-25 08:42:23', '2018-11-25 09:42:23');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_action_meta`
--

DROP TABLE IF EXISTS `lab_nf3_action_meta`;
CREATE TABLE IF NOT EXISTS `lab_nf3_action_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `key` longtext NOT NULL,
  `value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=83 ;

--
-- Dump dei dati per la tabella `lab_nf3_action_meta`
--

INSERT INTO `lab_nf3_action_meta` (`id`, `parent_id`, `key`, `value`) VALUES
(1, 1, 'label', 'Store Submission'),
(2, 1, 'objectType', 'Action'),
(3, 1, 'objectDomain', 'actions'),
(4, 1, 'editActive', ''),
(5, 1, 'conditions', 'a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}'),
(6, 1, 'payment_gateways', ''),
(7, 1, 'payment_total', ''),
(8, 1, 'tag', ''),
(9, 1, 'to', ''),
(10, 1, 'email_subject', ''),
(11, 1, 'email_message', ''),
(12, 1, 'from_name', ''),
(13, 1, 'from_address', ''),
(14, 1, 'reply_to', ''),
(15, 1, 'email_format', 'html'),
(16, 1, 'cc', ''),
(17, 1, 'bcc', ''),
(18, 1, 'attach_csv', ''),
(19, 1, 'redirect_url', ''),
(20, 1, 'email_message_plain', ''),
(21, 2, 'label', 'Email Confirmation'),
(22, 2, 'to', '{field:email}'),
(23, 2, 'subject', 'This is an email action.'),
(24, 2, 'message', 'Hello, Ninja Forms!'),
(25, 2, 'objectType', 'Action'),
(26, 2, 'objectDomain', 'actions'),
(27, 2, 'editActive', ''),
(28, 2, 'conditions', 'a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:0:{}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}'),
(29, 2, 'payment_gateways', ''),
(30, 2, 'payment_total', ''),
(31, 2, 'tag', ''),
(32, 2, 'email_subject', 'Submission Confirmation '),
(33, 2, 'email_message', '<p>{all_fields_table}<br></p>'),
(34, 2, 'from_name', ''),
(35, 2, 'from_address', ''),
(36, 2, 'reply_to', ''),
(37, 2, 'email_format', 'html'),
(38, 2, 'cc', ''),
(39, 2, 'bcc', ''),
(40, 2, 'attach_csv', ''),
(41, 2, 'email_message_plain', ''),
(42, 3, 'objectType', 'Action'),
(43, 3, 'objectDomain', 'actions'),
(44, 3, 'editActive', ''),
(45, 3, 'label', 'Email Notification'),
(46, 3, 'conditions', 'a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}'),
(47, 3, 'payment_gateways', ''),
(48, 3, 'payment_total', ''),
(49, 3, 'tag', ''),
(50, 3, 'to', '{system:admin_email}'),
(51, 3, 'email_subject', 'New message from {field:name}'),
(52, 3, 'email_message', '<p>{field:message}</p><p>-{field:name} ( {field:email} )</p>'),
(53, 3, 'from_name', ''),
(54, 3, 'from_address', ''),
(55, 3, 'reply_to', '{field:email}'),
(56, 3, 'email_format', 'html'),
(57, 3, 'cc', ''),
(58, 3, 'bcc', ''),
(59, 3, 'attach_csv', '0'),
(60, 3, 'email_message_plain', ''),
(61, 4, 'label', 'Success Message'),
(62, 4, 'message', 'Thank you {field:name} for filling out my form!'),
(63, 4, 'objectType', 'Action'),
(64, 4, 'objectDomain', 'actions'),
(65, 4, 'editActive', ''),
(66, 4, 'conditions', 'a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}'),
(67, 4, 'payment_gateways', ''),
(68, 4, 'payment_total', ''),
(69, 4, 'tag', ''),
(70, 4, 'to', ''),
(71, 4, 'email_subject', ''),
(72, 4, 'email_message', ''),
(73, 4, 'from_name', ''),
(74, 4, 'from_address', ''),
(75, 4, 'reply_to', ''),
(76, 4, 'email_format', 'html'),
(77, 4, 'cc', ''),
(78, 4, 'bcc', ''),
(79, 4, 'attach_csv', ''),
(80, 4, 'redirect_url', ''),
(81, 4, 'success_msg', '<p>Form submitted successfully.</p><p>A confirmation email was sent to {field:email}.</p>'),
(82, 4, 'email_message_plain', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_chunks`
--

DROP TABLE IF EXISTS `lab_nf3_chunks`;
CREATE TABLE IF NOT EXISTS `lab_nf3_chunks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_fields`
--

DROP TABLE IF EXISTS `lab_nf3_fields`;
CREATE TABLE IF NOT EXISTS `lab_nf3_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` longtext,
  `key` longtext,
  `type` longtext,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `lab_nf3_fields`
--

INSERT INTO `lab_nf3_fields` (`id`, `label`, `key`, `type`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 1, '2018-11-25 08:42:22', '2018-11-25 09:42:22'),
(2, NULL, NULL, NULL, 1, '2018-11-25 08:42:22', '2018-11-25 09:42:22'),
(3, NULL, NULL, NULL, 1, '2018-11-25 08:42:22', '2018-11-25 09:42:22'),
(4, NULL, NULL, NULL, 1, '2018-11-25 08:42:22', '2018-11-25 09:42:22');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_field_meta`
--

DROP TABLE IF EXISTS `lab_nf3_field_meta`;
CREATE TABLE IF NOT EXISTS `lab_nf3_field_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `key` longtext NOT NULL,
  `value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_forms`
--

DROP TABLE IF EXISTS `lab_nf3_forms`;
CREATE TABLE IF NOT EXISTS `lab_nf3_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext,
  `key` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `subs` int(11) DEFAULT NULL,
  `form_title` longtext,
  `default_label_pos` varchar(15) DEFAULT NULL,
  `show_title` bit(1) DEFAULT NULL,
  `clear_complete` bit(1) DEFAULT NULL,
  `hide_complete` bit(1) DEFAULT NULL,
  `logged_in` bit(1) DEFAULT NULL,
  `seq_num` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `lab_nf3_forms`
--

INSERT INTO `lab_nf3_forms` (`id`, `title`, `key`, `created_at`, `updated_at`, `views`, `subs`, `form_title`, `default_label_pos`, `show_title`, `clear_complete`, `hide_complete`, `logged_in`, `seq_num`) VALUES
(1, 'Contact Me', NULL, '2018-11-25 09:42:22', '2018-11-25 09:42:22', NULL, NULL, 'Contact Me', 'above', b'1', b'1', b'1', b'0', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_form_meta`
--

DROP TABLE IF EXISTS `lab_nf3_form_meta`;
CREATE TABLE IF NOT EXISTS `lab_nf3_form_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `key` longtext NOT NULL,
  `value` longtext,
  `meta_key` longtext,
  `meta_value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=99 ;

--
-- Dump dei dati per la tabella `lab_nf3_form_meta`
--

INSERT INTO `lab_nf3_form_meta` (`id`, `parent_id`, `key`, `value`, `meta_key`, `meta_value`) VALUES
(1, 1, 'key', '', 'key', ''),
(2, 1, 'created_at', '2018-11-25 09:42:22', 'created_at', '2018-11-25 09:42:22'),
(3, 1, 'default_label_pos', 'above', 'default_label_pos', 'above'),
(4, 1, 'conditions', 'a:0:{}', 'conditions', 'a:0:{}'),
(5, 1, 'objectType', 'Form Setting', 'objectType', 'Form Setting'),
(6, 1, 'editActive', '', 'editActive', ''),
(7, 1, 'show_title', '1', 'show_title', '1'),
(8, 1, 'clear_complete', '1', 'clear_complete', '1'),
(9, 1, 'hide_complete', '1', 'hide_complete', '1'),
(10, 1, 'wrapper_class', '', 'wrapper_class', ''),
(11, 1, 'element_class', '', 'element_class', ''),
(12, 1, 'add_submit', '1', 'add_submit', '1'),
(13, 1, 'logged_in', '', 'logged_in', ''),
(14, 1, 'not_logged_in_msg', '', 'not_logged_in_msg', ''),
(15, 1, 'sub_limit_number', '', 'sub_limit_number', ''),
(16, 1, 'sub_limit_msg', '', 'sub_limit_msg', ''),
(17, 1, 'calculations', 'a:0:{}', 'calculations', 'a:0:{}'),
(18, 1, 'formContentData', 'a:4:{i:0;a:2:{s:5:"order";s:1:"0";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:4:"name";}s:5:"width";s:3:"100";}}}i:1;a:2:{s:5:"order";s:1:"1";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:5:"email";}s:5:"width";s:3:"100";}}}i:2;a:2:{s:5:"order";s:1:"2";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:7:"message";}s:5:"width";s:3:"100";}}}i:3;a:2:{s:5:"order";s:1:"3";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:6:"submit";}s:5:"width";s:3:"100";}}}}', 'formContentData', 'a:4:{i:0;a:2:{s:5:"order";s:1:"0";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:4:"name";}s:5:"width";s:3:"100";}}}i:1;a:2:{s:5:"order";s:1:"1";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:5:"email";}s:5:"width";s:3:"100";}}}i:2;a:2:{s:5:"order";s:1:"2";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:7:"message";}s:5:"width";s:3:"100";}}}i:3;a:2:{s:5:"order";s:1:"3";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:6:"submit";}s:5:"width";s:3:"100";}}}}'),
(19, 1, 'container_styles_background-color', '', 'container_styles_background-color', ''),
(20, 1, 'container_styles_border', '', 'container_styles_border', ''),
(21, 1, 'container_styles_border-style', '', 'container_styles_border-style', ''),
(22, 1, 'container_styles_border-color', '', 'container_styles_border-color', ''),
(23, 1, 'container_styles_color', '', 'container_styles_color', ''),
(24, 1, 'container_styles_height', '', 'container_styles_height', ''),
(25, 1, 'container_styles_width', '', 'container_styles_width', ''),
(26, 1, 'container_styles_font-size', '', 'container_styles_font-size', ''),
(27, 1, 'container_styles_margin', '', 'container_styles_margin', ''),
(28, 1, 'container_styles_padding', '', 'container_styles_padding', ''),
(29, 1, 'container_styles_display', '', 'container_styles_display', ''),
(30, 1, 'container_styles_float', '', 'container_styles_float', ''),
(31, 1, 'container_styles_show_advanced_css', '0', 'container_styles_show_advanced_css', '0'),
(32, 1, 'container_styles_advanced', '', 'container_styles_advanced', ''),
(33, 1, 'title_styles_background-color', '', 'title_styles_background-color', ''),
(34, 1, 'title_styles_border', '', 'title_styles_border', ''),
(35, 1, 'title_styles_border-style', '', 'title_styles_border-style', ''),
(36, 1, 'title_styles_border-color', '', 'title_styles_border-color', ''),
(37, 1, 'title_styles_color', '', 'title_styles_color', ''),
(38, 1, 'title_styles_height', '', 'title_styles_height', ''),
(39, 1, 'title_styles_width', '', 'title_styles_width', ''),
(40, 1, 'title_styles_font-size', '', 'title_styles_font-size', ''),
(41, 1, 'title_styles_margin', '', 'title_styles_margin', ''),
(42, 1, 'title_styles_padding', '', 'title_styles_padding', ''),
(43, 1, 'title_styles_display', '', 'title_styles_display', ''),
(44, 1, 'title_styles_float', '', 'title_styles_float', ''),
(45, 1, 'title_styles_show_advanced_css', '0', 'title_styles_show_advanced_css', '0'),
(46, 1, 'title_styles_advanced', '', 'title_styles_advanced', ''),
(47, 1, 'row_styles_background-color', '', 'row_styles_background-color', ''),
(48, 1, 'row_styles_border', '', 'row_styles_border', ''),
(49, 1, 'row_styles_border-style', '', 'row_styles_border-style', ''),
(50, 1, 'row_styles_border-color', '', 'row_styles_border-color', ''),
(51, 1, 'row_styles_color', '', 'row_styles_color', ''),
(52, 1, 'row_styles_height', '', 'row_styles_height', ''),
(53, 1, 'row_styles_width', '', 'row_styles_width', ''),
(54, 1, 'row_styles_font-size', '', 'row_styles_font-size', ''),
(55, 1, 'row_styles_margin', '', 'row_styles_margin', ''),
(56, 1, 'row_styles_padding', '', 'row_styles_padding', ''),
(57, 1, 'row_styles_display', '', 'row_styles_display', ''),
(58, 1, 'row_styles_show_advanced_css', '0', 'row_styles_show_advanced_css', '0'),
(59, 1, 'row_styles_advanced', '', 'row_styles_advanced', ''),
(60, 1, 'row-odd_styles_background-color', '', 'row-odd_styles_background-color', ''),
(61, 1, 'row-odd_styles_border', '', 'row-odd_styles_border', ''),
(62, 1, 'row-odd_styles_border-style', '', 'row-odd_styles_border-style', ''),
(63, 1, 'row-odd_styles_border-color', '', 'row-odd_styles_border-color', ''),
(64, 1, 'row-odd_styles_color', '', 'row-odd_styles_color', ''),
(65, 1, 'row-odd_styles_height', '', 'row-odd_styles_height', ''),
(66, 1, 'row-odd_styles_width', '', 'row-odd_styles_width', ''),
(67, 1, 'row-odd_styles_font-size', '', 'row-odd_styles_font-size', ''),
(68, 1, 'row-odd_styles_margin', '', 'row-odd_styles_margin', ''),
(69, 1, 'row-odd_styles_padding', '', 'row-odd_styles_padding', ''),
(70, 1, 'row-odd_styles_display', '', 'row-odd_styles_display', ''),
(71, 1, 'row-odd_styles_show_advanced_css', '0', 'row-odd_styles_show_advanced_css', '0'),
(72, 1, 'row-odd_styles_advanced', '', 'row-odd_styles_advanced', ''),
(73, 1, 'success-msg_styles_background-color', '', 'success-msg_styles_background-color', ''),
(74, 1, 'success-msg_styles_border', '', 'success-msg_styles_border', ''),
(75, 1, 'success-msg_styles_border-style', '', 'success-msg_styles_border-style', ''),
(76, 1, 'success-msg_styles_border-color', '', 'success-msg_styles_border-color', ''),
(77, 1, 'success-msg_styles_color', '', 'success-msg_styles_color', ''),
(78, 1, 'success-msg_styles_height', '', 'success-msg_styles_height', ''),
(79, 1, 'success-msg_styles_width', '', 'success-msg_styles_width', ''),
(80, 1, 'success-msg_styles_font-size', '', 'success-msg_styles_font-size', ''),
(81, 1, 'success-msg_styles_margin', '', 'success-msg_styles_margin', ''),
(82, 1, 'success-msg_styles_padding', '', 'success-msg_styles_padding', ''),
(83, 1, 'success-msg_styles_display', '', 'success-msg_styles_display', ''),
(84, 1, 'success-msg_styles_show_advanced_css', '0', 'success-msg_styles_show_advanced_css', '0'),
(85, 1, 'success-msg_styles_advanced', '', 'success-msg_styles_advanced', ''),
(86, 1, 'error_msg_styles_background-color', '', 'error_msg_styles_background-color', ''),
(87, 1, 'error_msg_styles_border', '', 'error_msg_styles_border', ''),
(88, 1, 'error_msg_styles_border-style', '', 'error_msg_styles_border-style', ''),
(89, 1, 'error_msg_styles_border-color', '', 'error_msg_styles_border-color', ''),
(90, 1, 'error_msg_styles_color', '', 'error_msg_styles_color', ''),
(91, 1, 'error_msg_styles_height', '', 'error_msg_styles_height', ''),
(92, 1, 'error_msg_styles_width', '', 'error_msg_styles_width', ''),
(93, 1, 'error_msg_styles_font-size', '', 'error_msg_styles_font-size', ''),
(94, 1, 'error_msg_styles_margin', '', 'error_msg_styles_margin', ''),
(95, 1, 'error_msg_styles_padding', '', 'error_msg_styles_padding', ''),
(96, 1, 'error_msg_styles_display', '', 'error_msg_styles_display', ''),
(97, 1, 'error_msg_styles_show_advanced_css', '0', 'error_msg_styles_show_advanced_css', '0'),
(98, 1, 'error_msg_styles_advanced', '', 'error_msg_styles_advanced', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_objects`
--

DROP TABLE IF EXISTS `lab_nf3_objects`;
CREATE TABLE IF NOT EXISTS `lab_nf3_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext,
  `title` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_object_meta`
--

DROP TABLE IF EXISTS `lab_nf3_object_meta`;
CREATE TABLE IF NOT EXISTS `lab_nf3_object_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `key` longtext NOT NULL,
  `value` longtext,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_relationships`
--

DROP TABLE IF EXISTS `lab_nf3_relationships`;
CREATE TABLE IF NOT EXISTS `lab_nf3_relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `child_type` longtext NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_type` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_nf3_upgrades`
--

DROP TABLE IF EXISTS `lab_nf3_upgrades`;
CREATE TABLE IF NOT EXISTS `lab_nf3_upgrades` (
  `id` int(11) NOT NULL,
  `cache` longtext,
  `stage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `lab_nf3_upgrades`
--

INSERT INTO `lab_nf3_upgrades` (`id`, `cache`, `stage`) VALUES
(1, 'a:4:{s:2:"id";i:1;s:6:"fields";a:4:{i:0;a:2:{s:2:"id";i:1;s:8:"settings";a:70:{s:5:"label";s:4:"Name";s:3:"key";s:4:"name";s:9:"parent_id";i:1;s:4:"type";s:7:"textbox";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"1";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"input_limit";s:0:"";s:16:"input_limit_type";s:10:"characters";s:15:"input_limit_msg";s:17:"Character(s) left";s:10:"manual_key";s:0:"";s:13:"disable_input";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"disable_browser_autocomplete";s:0:"";s:4:"mask";s:0:"";s:11:"custom_mask";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3277";}}i:1;a:2:{s:2:"id";i:2;s:8:"settings";a:62:{s:5:"label";s:5:"Email";s:3:"key";s:5:"email";s:9:"parent_id";i:1;s:4:"type";s:5:"email";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"2";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3281";}}i:2;a:2:{s:2:"id";i:3;s:8:"settings";a:71:{s:5:"label";s:7:"Message";s:3:"key";s:7:"message";s:9:"parent_id";i:1;s:4:"type";s:8:"textarea";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"3";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"input_limit";s:0:"";s:16:"input_limit_type";s:10:"characters";s:15:"input_limit_msg";s:17:"Character(s) left";s:10:"manual_key";s:0:"";s:13:"disable_input";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"disable_browser_autocomplete";s:0:"";s:12:"textarea_rte";s:0:"";s:18:"disable_rte_mobile";s:0:"";s:14:"textarea_media";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3284";}}i:3;a:2:{s:2:"id";i:4;s:8:"settings";a:69:{s:5:"label";s:6:"Submit";s:3:"key";s:6:"submit";s:9:"parent_id";i:1;s:4:"type";s:6:"submit";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:16:"processing_label";s:10:"Processing";s:5:"order";s:1:"5";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:13:"element_class";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:44:"submit_element_hover_styles_background-color";s:0:"";s:34:"submit_element_hover_styles_border";s:0:"";s:40:"submit_element_hover_styles_border-style";s:0:"";s:40:"submit_element_hover_styles_border-color";s:0:"";s:33:"submit_element_hover_styles_color";s:0:"";s:34:"submit_element_hover_styles_height";s:0:"";s:33:"submit_element_hover_styles_width";s:0:"";s:37:"submit_element_hover_styles_font-size";s:0:"";s:34:"submit_element_hover_styles_margin";s:0:"";s:35:"submit_element_hover_styles_padding";s:0:"";s:35:"submit_element_hover_styles_display";s:0:"";s:33:"submit_element_hover_styles_float";s:0:"";s:45:"submit_element_hover_styles_show_advanced_css";s:1:"0";s:36:"submit_element_hover_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3287";}}}s:7:"actions";a:4:{i:0;a:2:{s:2:"id";i:1;s:8:"settings";a:25:{s:5:"title";s:0:"";s:3:"key";s:0:"";s:4:"type";s:4:"save";s:6:"active";s:1:"1";s:10:"created_at";s:19:"2018-11-25 09:42:23";s:5:"label";s:16:"Store Submission";s:10:"objectType";s:6:"Action";s:12:"objectDomain";s:7:"actions";s:10:"editActive";s:0:"";s:10:"conditions";a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}s:16:"payment_gateways";s:0:"";s:13:"payment_total";s:0:"";s:3:"tag";s:0:"";s:2:"to";s:0:"";s:13:"email_subject";s:0:"";s:13:"email_message";s:0:"";s:9:"from_name";s:0:"";s:12:"from_address";s:0:"";s:8:"reply_to";s:0:"";s:12:"email_format";s:4:"html";s:2:"cc";s:0:"";s:3:"bcc";s:0:"";s:10:"attach_csv";s:0:"";s:12:"redirect_url";s:0:"";s:19:"email_message_plain";s:0:"";}}i:1;a:2:{s:2:"id";i:2;s:8:"settings";a:26:{s:5:"title";s:0:"";s:3:"key";s:0:"";s:4:"type";s:5:"email";s:6:"active";s:1:"1";s:10:"created_at";s:19:"2018-11-25 09:42:23";s:5:"label";s:18:"Email Confirmation";s:2:"to";s:13:"{field:email}";s:7:"subject";s:24:"This is an email action.";s:7:"message";s:19:"Hello, Ninja Forms!";s:10:"objectType";s:6:"Action";s:12:"objectDomain";s:7:"actions";s:10:"editActive";s:0:"";s:10:"conditions";a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:0:{}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}s:16:"payment_gateways";s:0:"";s:13:"payment_total";s:0:"";s:3:"tag";s:0:"";s:13:"email_subject";s:24:"Submission Confirmation ";s:13:"email_message";s:29:"<p>{all_fields_table}<br></p>";s:9:"from_name";s:0:"";s:12:"from_address";s:0:"";s:8:"reply_to";s:0:"";s:12:"email_format";s:4:"html";s:2:"cc";s:0:"";s:3:"bcc";s:0:"";s:10:"attach_csv";s:0:"";s:19:"email_message_plain";s:0:"";}}i:2;a:2:{s:2:"id";i:3;s:8:"settings";a:24:{s:5:"title";s:0:"";s:3:"key";s:0:"";s:4:"type";s:5:"email";s:6:"active";s:1:"1";s:10:"created_at";s:19:"2018-11-25 09:42:23";s:10:"objectType";s:6:"Action";s:12:"objectDomain";s:7:"actions";s:10:"editActive";s:0:"";s:5:"label";s:18:"Email Notification";s:10:"conditions";a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}s:16:"payment_gateways";s:0:"";s:13:"payment_total";s:0:"";s:3:"tag";s:0:"";s:2:"to";s:20:"{system:admin_email}";s:13:"email_subject";s:29:"New message from {field:name}";s:13:"email_message";s:60:"<p>{field:message}</p><p>-{field:name} ( {field:email} )</p>";s:9:"from_name";s:0:"";s:12:"from_address";s:0:"";s:8:"reply_to";s:13:"{field:email}";s:12:"email_format";s:4:"html";s:2:"cc";s:0:"";s:3:"bcc";s:0:"";s:10:"attach_csv";s:1:"0";s:19:"email_message_plain";s:0:"";}}i:3;a:2:{s:2:"id";i:4;s:8:"settings";a:27:{s:5:"title";s:0:"";s:3:"key";s:0:"";s:4:"type";s:14:"successmessage";s:6:"active";s:1:"1";s:10:"created_at";s:19:"2018-11-25 09:42:23";s:5:"label";s:15:"Success Message";s:7:"message";s:47:"Thank you {field:name} for filling out my form!";s:10:"objectType";s:6:"Action";s:12:"objectDomain";s:7:"actions";s:10:"editActive";s:0:"";s:10:"conditions";a:6:{s:9:"collapsed";s:0:"";s:7:"process";s:1:"1";s:9:"connector";s:3:"all";s:4:"when";a:1:{i:0;a:6:{s:9:"connector";s:3:"AND";s:3:"key";s:0:"";s:10:"comparator";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"when";}}s:4:"then";a:1:{i:0;a:5:{s:3:"key";s:0:"";s:7:"trigger";s:0:"";s:5:"value";s:0:"";s:4:"type";s:5:"field";s:9:"modelType";s:4:"then";}}s:4:"else";a:0:{}}s:16:"payment_gateways";s:0:"";s:13:"payment_total";s:0:"";s:3:"tag";s:0:"";s:2:"to";s:0:"";s:13:"email_subject";s:0:"";s:13:"email_message";s:0:"";s:9:"from_name";s:0:"";s:12:"from_address";s:0:"";s:8:"reply_to";s:0:"";s:12:"email_format";s:4:"html";s:2:"cc";s:0:"";s:3:"bcc";s:0:"";s:10:"attach_csv";s:0:"";s:12:"redirect_url";s:0:"";s:11:"success_msg";s:89:"<p>Form submitted successfully.</p><p>A confirmation email was sent to {field:email}.</p>";s:19:"email_message_plain";s:0:"";}}}s:8:"settings";a:99:{s:5:"title";s:10:"Contact Me";s:3:"key";s:0:"";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:17:"default_label_pos";s:5:"above";s:10:"conditions";a:0:{}s:10:"objectType";s:12:"Form Setting";s:10:"editActive";s:0:"";s:10:"show_title";s:1:"1";s:14:"clear_complete";s:1:"1";s:13:"hide_complete";s:1:"1";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"add_submit";s:1:"1";s:9:"logged_in";s:0:"";s:17:"not_logged_in_msg";s:0:"";s:16:"sub_limit_number";s:0:"";s:13:"sub_limit_msg";s:0:"";s:12:"calculations";a:0:{}s:15:"formContentData";a:4:{i:0;a:2:{s:5:"order";s:1:"0";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:4:"name";}s:5:"width";s:3:"100";}}}i:1;a:2:{s:5:"order";s:1:"1";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:5:"email";}s:5:"width";s:3:"100";}}}i:2;a:2:{s:5:"order";s:1:"2";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:7:"message";}s:5:"width";s:3:"100";}}}i:3;a:2:{s:5:"order";s:1:"3";s:5:"cells";a:1:{i:0;a:3:{s:5:"order";s:1:"0";s:6:"fields";a:1:{i:0;s:6:"submit";}s:5:"width";s:3:"100";}}}}s:33:"container_styles_background-color";s:0:"";s:23:"container_styles_border";s:0:"";s:29:"container_styles_border-style";s:0:"";s:29:"container_styles_border-color";s:0:"";s:22:"container_styles_color";s:0:"";s:23:"container_styles_height";s:0:"";s:22:"container_styles_width";s:0:"";s:26:"container_styles_font-size";s:0:"";s:23:"container_styles_margin";s:0:"";s:24:"container_styles_padding";s:0:"";s:24:"container_styles_display";s:0:"";s:22:"container_styles_float";s:0:"";s:34:"container_styles_show_advanced_css";s:1:"0";s:25:"container_styles_advanced";s:0:"";s:29:"title_styles_background-color";s:0:"";s:19:"title_styles_border";s:0:"";s:25:"title_styles_border-style";s:0:"";s:25:"title_styles_border-color";s:0:"";s:18:"title_styles_color";s:0:"";s:19:"title_styles_height";s:0:"";s:18:"title_styles_width";s:0:"";s:22:"title_styles_font-size";s:0:"";s:19:"title_styles_margin";s:0:"";s:20:"title_styles_padding";s:0:"";s:20:"title_styles_display";s:0:"";s:18:"title_styles_float";s:0:"";s:30:"title_styles_show_advanced_css";s:1:"0";s:21:"title_styles_advanced";s:0:"";s:27:"row_styles_background-color";s:0:"";s:17:"row_styles_border";s:0:"";s:23:"row_styles_border-style";s:0:"";s:23:"row_styles_border-color";s:0:"";s:16:"row_styles_color";s:0:"";s:17:"row_styles_height";s:0:"";s:16:"row_styles_width";s:0:"";s:20:"row_styles_font-size";s:0:"";s:17:"row_styles_margin";s:0:"";s:18:"row_styles_padding";s:0:"";s:18:"row_styles_display";s:0:"";s:28:"row_styles_show_advanced_css";s:1:"0";s:19:"row_styles_advanced";s:0:"";s:31:"row-odd_styles_background-color";s:0:"";s:21:"row-odd_styles_border";s:0:"";s:27:"row-odd_styles_border-style";s:0:"";s:27:"row-odd_styles_border-color";s:0:"";s:20:"row-odd_styles_color";s:0:"";s:21:"row-odd_styles_height";s:0:"";s:20:"row-odd_styles_width";s:0:"";s:24:"row-odd_styles_font-size";s:0:"";s:21:"row-odd_styles_margin";s:0:"";s:22:"row-odd_styles_padding";s:0:"";s:22:"row-odd_styles_display";s:0:"";s:32:"row-odd_styles_show_advanced_css";s:1:"0";s:23:"row-odd_styles_advanced";s:0:"";s:35:"success-msg_styles_background-color";s:0:"";s:25:"success-msg_styles_border";s:0:"";s:31:"success-msg_styles_border-style";s:0:"";s:31:"success-msg_styles_border-color";s:0:"";s:24:"success-msg_styles_color";s:0:"";s:25:"success-msg_styles_height";s:0:"";s:24:"success-msg_styles_width";s:0:"";s:28:"success-msg_styles_font-size";s:0:"";s:25:"success-msg_styles_margin";s:0:"";s:26:"success-msg_styles_padding";s:0:"";s:26:"success-msg_styles_display";s:0:"";s:36:"success-msg_styles_show_advanced_css";s:1:"0";s:27:"success-msg_styles_advanced";s:0:"";s:33:"error_msg_styles_background-color";s:0:"";s:23:"error_msg_styles_border";s:0:"";s:29:"error_msg_styles_border-style";s:0:"";s:29:"error_msg_styles_border-color";s:0:"";s:22:"error_msg_styles_color";s:0:"";s:23:"error_msg_styles_height";s:0:"";s:22:"error_msg_styles_width";s:0:"";s:26:"error_msg_styles_font-size";s:0:"";s:23:"error_msg_styles_margin";s:0:"";s:24:"error_msg_styles_padding";s:0:"";s:24:"error_msg_styles_display";s:0:"";s:34:"error_msg_styles_show_advanced_css";s:1:"0";s:25:"error_msg_styles_advanced";s:0:"";}}', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_ninja_forms_uploads`
--

DROP TABLE IF EXISTS `lab_ninja_forms_uploads`;
CREATE TABLE IF NOT EXISTS `lab_ninja_forms_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `form_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8 NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_opanda_leads`
--

DROP TABLE IF EXISTS `lab_opanda_leads`;
CREATE TABLE IF NOT EXISTS `lab_opanda_leads` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lead_display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_family` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lead_date` int(11) NOT NULL,
  `lead_email_confirmed` int(1) NOT NULL DEFAULT '0' COMMENT 'email',
  `lead_subscription_confirmed` int(1) NOT NULL DEFAULT '0' COMMENT 'subscription',
  `lead_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_item_id` int(11) DEFAULT NULL,
  `lead_post_id` int(11) DEFAULT NULL,
  `lead_item_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_post_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_referer` text COLLATE utf8mb4_unicode_ci,
  `lead_confirmation_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_temp` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lead_email` (`lead_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_opanda_leads_fields`
--

DROP TABLE IF EXISTS `lab_opanda_leads_fields`;
CREATE TABLE IF NOT EXISTS `lab_opanda_leads_fields` (
  `lead_id` int(10) unsigned NOT NULL,
  `field_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_custom` bit(1) NOT NULL DEFAULT b'0',
  UNIQUE KEY `UK_wp_opanda_leads_fields` (`lead_id`,`field_name`),
  KEY `IDX_wp_opanda_leads_fields_field_name` (`field_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_opanda_stats_v2`
--

DROP TABLE IF EXISTS `lab_opanda_stats_v2`;
CREATE TABLE IF NOT EXISTS `lab_opanda_stats_v2` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `aggregate_date` date NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `metric_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metric_value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_opanda_stats_v2` (`aggregate_date`,`item_id`,`post_id`,`metric_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_options`
--

DROP TABLE IF EXISTS `lab_options`;
CREATE TABLE IF NOT EXISTS `lab_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=978 ;

--
-- Dump dei dati per la tabella `lab_options`
--

INSERT INTO `lab_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://www.mbsn.es', 'yes'),
(2, 'home', 'https://www.mbsn.es', 'yes'),
(3, 'blogname', 'marcomasoero', 'yes'),
(4, 'blogdescription', 'marcomasoero (lab)', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'antonio@maisto.info', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:227:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:10:"project/?$";s:27:"index.php?post_type=project";s:40:"project/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=project&feed=$matches[1]";s:35:"project/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=project&feed=$matches[1]";s:27:"project/page/([0-9]{1,})/?$";s:45:"index.php?post_type=project&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:34:"nf_sub/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"nf_sub/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"nf_sub/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"nf_sub/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"nf_sub/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"nf_sub/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"nf_sub/([^/]+)/embed/?$";s:39:"index.php?nf_sub=$matches[1]&embed=true";s:27:"nf_sub/([^/]+)/trackback/?$";s:33:"index.php?nf_sub=$matches[1]&tb=1";s:35:"nf_sub/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?nf_sub=$matches[1]&paged=$matches[2]";s:42:"nf_sub/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?nf_sub=$matches[1]&cpage=$matches[2]";s:32:"nf_sub/([^/]+)/wc-api(/(.*))?/?$";s:47:"index.php?nf_sub=$matches[1]&wc-api=$matches[3]";s:38:"nf_sub/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:49:"nf_sub/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:31:"nf_sub/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?nf_sub=$matches[1]&page=$matches[2]";s:23:"nf_sub/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"nf_sub/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"nf_sub/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"nf_sub/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"nf_sub/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"nf_sub/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:55:"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:50:"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:31:"product-category/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:43:"product-category/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:25:"product-category/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:52:"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:47:"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:28:"product-tag/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:40:"product-tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:22:"product-tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"product/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"product/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"product/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"product/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"product/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:36:"product/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"product/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"product/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"product/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"product/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"product/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"product/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:35:"project/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"project/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"project/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"project/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"project/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"project/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"project/([^/]+)/embed/?$";s:40:"index.php?project=$matches[1]&embed=true";s:28:"project/([^/]+)/trackback/?$";s:34:"index.php?project=$matches[1]&tb=1";s:48:"project/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?project=$matches[1]&feed=$matches[2]";s:43:"project/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?project=$matches[1]&feed=$matches[2]";s:36:"project/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?project=$matches[1]&paged=$matches[2]";s:43:"project/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?project=$matches[1]&cpage=$matches[2]";s:33:"project/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?project=$matches[1]&wc-api=$matches[3]";s:39:"project/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"project/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"project/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?project=$matches[1]&page=$matches[2]";s:24:"project/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"project/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"project/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"project/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"project/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"project/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:57:"project_category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?project_category=$matches[1]&feed=$matches[2]";s:52:"project_category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?project_category=$matches[1]&feed=$matches[2]";s:33:"project_category/([^/]+)/embed/?$";s:49:"index.php?project_category=$matches[1]&embed=true";s:45:"project_category/([^/]+)/page/?([0-9]{1,})/?$";s:56:"index.php?project_category=$matches[1]&paged=$matches[2]";s:27:"project_category/([^/]+)/?$";s:38:"index.php?project_category=$matches[1]";s:52:"project_tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?project_tag=$matches[1]&feed=$matches[2]";s:47:"project_tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?project_tag=$matches[1]&feed=$matches[2]";s:28:"project_tag/([^/]+)/embed/?$";s:44:"index.php?project_tag=$matches[1]&embed=true";s:40:"project_tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?project_tag=$matches[1]&paged=$matches[2]";s:22:"project_tag/([^/]+)/?$";s:33:"index.php?project_tag=$matches[1]";s:45:"amn_exact-metrics/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"amn_exact-metrics/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"amn_exact-metrics/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"amn_exact-metrics/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"amn_exact-metrics/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"amn_exact-metrics/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"amn_exact-metrics/([^/]+)/embed/?$";s:50:"index.php?amn_exact-metrics=$matches[1]&embed=true";s:38:"amn_exact-metrics/([^/]+)/trackback/?$";s:44:"index.php?amn_exact-metrics=$matches[1]&tb=1";s:46:"amn_exact-metrics/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?amn_exact-metrics=$matches[1]&paged=$matches[2]";s:53:"amn_exact-metrics/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?amn_exact-metrics=$matches[1]&cpage=$matches[2]";s:43:"amn_exact-metrics/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?amn_exact-metrics=$matches[1]&wc-api=$matches[3]";s:49:"amn_exact-metrics/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"amn_exact-metrics/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"amn_exact-metrics/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?amn_exact-metrics=$matches[1]&page=$matches[2]";s:34:"amn_exact-metrics/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"amn_exact-metrics/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"amn_exact-metrics/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"amn_exact-metrics/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"amn_exact-metrics/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"amn_exact-metrics/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:62:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$";s:99:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]";s:62:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:73:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:21:{i:0;s:19:"akismet/akismet.php";i:1;s:15:"bloom/bloom.php";i:2;s:25:"cookie-bar/cookie-bar.php";i:3;s:29:"divi-builder/divi-builder.php";i:4;s:33:"duplicate-post/duplicate-post.php";i:5;s:45:"enable-media-replace/enable-media-replace.php";i:6;s:43:"export-users-to-csv/export-users-to-csv.php";i:7;s:43:"google-analytics-dashboard-for-wp/gadwp.php";i:8;s:19:"monarch/monarch.php";i:9;s:57:"ninja-forms-paypal-express/ninja-forms-paypal-express.php";i:10;s:53:"ninja-forms-table-editor/ninja-forms-table-editor.php";i:11;s:36:"ninja-forms-uploads/file-uploads.php";i:12;s:27:"ninja-forms/ninja-forms.php";i:13;s:31:"sg-cachepress/sg-cachepress.php";i:14;s:47:"sociallocker-next-premium/sociallocker-next.php";i:15;s:61:"woo-orders-date-range-filter/woo-orders-date-range-filter.php";i:16;s:34:"woocommerce-advanced-gift/main.php";i:17;s:42:"woocommerce-menu-bar-cart/wp-menu-cart.php";i:18;s:80:"woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php";i:19;s:27:"woocommerce/woocommerce.php";i:20;s:41:"yith-woocommerce-product-bundles/init.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'bizworx', 'yes'),
(41, 'stylesheet', 'bizworx', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:43:"google-analytics-dashboard-for-wp/gadwp.php";a:2:{i:0;s:15:"GADWP_Uninstall";i:1;s:9:"uninstall";}s:27:"ninja-forms/ninja-forms.php";s:21:"ninja_forms_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'lab_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:122:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:10:"copy_posts";b:1;s:16:"edit_opanda-item";b:1;s:16:"read_opanda-item";b:1;s:18:"delete_opanda-item";b:1;s:17:"edit_opanda-items";b:1;s:24:"edit_others_opanda-items";b:1;s:20:"publish_opanda-items";b:1;s:25:"read_private_opanda-items";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:10:"copy_posts";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop manager";s:12:"capabilities";a:93:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"edit_theme_options";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;}}}', 'yes'),
(95, 'fresh_site', '1', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:5:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'cron', 'a:22:{i:1544030571;a:1:{s:26:"action_scheduler_run_queue";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:12:"every_minute";s:4:"args";a:0:{}s:8:"interval";i:60;}}}i:1544030764;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1544033531;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1544036155;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"26e9e6ff95b3d23cf3d0202a8d7dfd36";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:22;}}}}i:1544036184;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"24daa8f49584a6f1b1395d475758a1de";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:23;}}}}i:1544037068;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"799be243d6827cc0bc0951691920a50b";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:24;}}}}i:1544037120;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"2af2e0878689b7c2fa62ccf0765f5768";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:25;}}}}i:1544037149;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"4334b88fe3501b2f0e4f630eb24f7ca5";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:26;}}}}i:1544038527;a:1:{s:33:"woocommerce_cleanup_personal_data";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544038537;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544049327;a:1:{s:24:"woocommerce_cleanup_logs";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544054400;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544060127;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1544066764;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1544073217;a:1:{s:36:"onp_check_upadates_sociallocker-next";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1544087951;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544116390;a:1:{s:24:"bloom_lists_auto_refresh";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544348661;a:1:{s:22:"nf_marketing_feed_cron";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:9:"nf-weekly";s:4:"args";a:0:{}s:8:"interval";i:604800;}}}i:1545817461;a:1:{s:13:"nf_optin_cron";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"nf-monthly";s:4:"args";a:0:{}s:8:"interval";i:2678400;}}}i:1546300800;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}i:1546621977;a:1:{s:32:"et_core_page_resource_auto_clear";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:1:{s:18:"custom_css_post_id";i:-1;}', 'yes'),
(114, '_transient_twentyseventeen_categories', '1', 'yes'),
(123, 'theme_mods_bizworx', 'a:2:{s:18:"custom_css_post_id";i:-1;s:39:"et_updated_layouts_built_for_post_types";s:3:"yes";}', 'yes'),
(116, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.8.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.8.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.8";s:7:"version";s:5:"4.9.8";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1544029951;s:15:"version_checked";s:5:"4.9.8";s:12:"translations";a:0:{}}', 'no'),
(121, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1544029952;s:7:"checked";a:4:{s:7:"bizworx";s:5:"0.0.4";s:13:"twentyfifteen";s:3:"2.0";s:15:"twentyseventeen";s:3:"1.7";s:13:"twentysixteen";s:3:"1.5";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no');
INSERT INTO `lab_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(242, 'fl_notifications', 'a:3:{s:4:"read";b:1;s:8:"checksum";a:8:{i:408945;i:2968285658;i:404849;i:308115498;i:375859;i:1690867192;i:352086;i:1712285855;i:347444;i:2016001167;i:344408;i:4147876357;i:337380;i:61041398;i:333965;i:4130695;}s:4:"data";s:15202:"[{"id":408945,"date":"2018-11-23T15:19:24","date_gmt":"2018-11-23T23:19:24","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=408945"},"modified":"2018-11-23T15:20:13","modified_gmt":"2018-11-23T23:20:13","slug":"black-friday-cyber-monday-25-off-everything","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=black-friday-cyber-monday-25-off-everything","title":{"rendered":"Black Friday \\/ Cyber Monday \\u2013 25% Off Everything"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-408954\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/black-friday-notification.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/black-friday-notification.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/black-friday-notification-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>Happy Turkey Week! We''re excited to continue our tradition of offering up a major discount on\\u00a0Beaver Builder for Black Friday and Cyber Monday 2018. Don''t miss out on this limited time offer.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/black-friday-cyber-monday-2018-25-off-everything\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/408945"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=408945"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":404849,"date":"2018-11-08T13:34:29","date_gmt":"2018-11-08T21:34:29","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=404849"},"modified":"2018-11-08T13:34:29","modified_gmt":"2018-11-08T21:34:29","slug":"autumn-update-addressing-all-the-elephants","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=autumn-update-addressing-all-the-elephants","title":{"rendered":"Autumn Update \\u2013 Addressing all the Elephants"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-404853\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/autumn-update-notification.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/autumn-update-notification.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/11\\/autumn-update-notification-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>Why, hello there! It''s an exciting time in the WordPress world with Gutenberg on the verge of going live. We have a bunch of exciting news to share and new features to announce. Find out more in our Autumn update.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/autumn-update-addressing-all-the-elephants\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/404849"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=404849"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":375859,"date":"2018-07-11T12:30:14","date_gmt":"2018-07-11T19:30:14","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=375859"},"modified":"2018-07-11T12:30:14","modified_gmt":"2018-07-11T19:30:14","slug":"summer-update-sneak-peaks-new-features-and-more","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=summer-update-sneak-peaks-new-features-and-more","title":{"rendered":"Summer Update \\u2013 Sneak Peaks, New Features, and More"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-375874\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/07\\/bb-summer-2018.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/07\\/bb-summer-2018.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/07\\/bb-summer-2018-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>Hope you''re having a great summer! We have updates and alpha versions across the board\\u2014Page Builder, Beaver Themer, and the BB Theme are all getting some exciting new features and enhancements.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/summer-update-sneak-peaks-new-features-and-more\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/375859"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=375859"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":352086,"date":"2018-04-26T11:28:42","date_gmt":"2018-04-26T18:28:42","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=352086"},"modified":"2018-04-26T11:28:42","modified_gmt":"2018-04-26T18:28:42","slug":"happy-4th-birthday-to-beaver-builder","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=happy-4th-birthday-to-beaver-builder","title":{"rendered":"Happy 4th Birthday to Beaver Builder"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-352087\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/bday-notification.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/bday-notification.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/bday-notification-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>For the last few years, we\\u2019ve made a point to celebrate our birthday here at\\u00a0Beaver Builder. It\\u2019s hard to believe, since starting this journey from client work to products, a whole four years have gone by.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/happy-4th-birthday-to-beaver-builder\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/352086"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=352086"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":347444,"date":"2018-04-04T16:27:13","date_gmt":"2018-04-04T23:27:13","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=347444"},"modified":"2018-04-04T16:27:13","modified_gmt":"2018-04-04T23:27:13","slug":"beaver-builder-2-1-redridge","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=beaver-builder-2-1-redridge","title":{"rendered":"Beaver Builder 2.1 &#8220;Redridge&#8221;"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-347445\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/redridge-notification-normal.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/redridge-notification-normal.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/04\\/redridge-notification-normal-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>Beaver Builder 2.1 &#8220;Redridge&#8221; introduces a TON of new features and workflow improvements. If you''re reading this, our new notification center here is one of them! Redridge also brings inline editing, saved columns, Gutenberg support, code validation, and a whole lot more. Click here to check out our release post and learn about all the new goodies.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/beaver-builder-2-1-redridge\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/347444"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=347444"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":344408,"date":"2018-03-22T10:40:12","date_gmt":"2018-03-22T17:40:12","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=344408"},"modified":"2018-03-22T10:51:16","modified_gmt":"2018-03-22T17:51:16","slug":"beaver-builder-team-ask-us-anything","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=beaver-builder-team-ask-us-anything","title":{"rendered":"Beaver Builder Team Ask Us Anything"},"content":{"rendered":"<p><img class=\\"aligncenter size-full wp-image-344409\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/03\\/ama-notification.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/03\\/ama-notification.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/03\\/ama-notification-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>In our latest update, Billy, Brent, Justin, and Robby sit down and answer your questions in video format! We also discuss the 2.1 alpha (which you''re probably aware of if you''re reading this notification\\u00a0\\ud83d\\ude02) and a lot more&#8230;<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/ask-us-anything-ama-update-post\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/344408"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=344408"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":337380,"date":"2018-02-21T10:41:52","date_gmt":"2018-02-21T18:41:52","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=337380"},"modified":"2018-02-21T10:41:52","modified_gmt":"2018-02-21T18:41:52","slug":"48-websites-48-hours","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=48-websites-48-hours","title":{"rendered":"48 Websites in 48 Hours"},"content":{"rendered":"<p><img class=\\"size-full wp-image-337381 aligncenter\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/02\\/48in48-case-study-notification.jpg\\" alt=\\"\\" width=\\"318\\" height=\\"139\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/02\\/48in48-case-study-notification.jpg 318w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/02\\/48in48-case-study-notification-300x131.jpg 300w\\" sizes=\\"(max-width: 318px) 100vw, 318px\\" \\/><\\/p>\\n<p>Check out this story about how our friends at 48in48 use\\u00a0Beaver Builder to create 48 nonprofit websites in 48 hours at their events hosted throughout the world.<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com\\/48in48-case-study-building-48-websites-nonprofits-48-hours\\/"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/337380"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=337380"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}},{"id":333965,"date":"2018-02-06T11:54:04","date_gmt":"2018-02-06T19:54:04","guid":{"rendered":"https:\\/\\/www.wpbeaverbuilder.com\\/?post_type=fl_notification&#038;p=333965"},"modified":"2018-02-15T12:09:49","modified_gmt":"2018-02-15T20:09:49","slug":"welcome-2-1-alpha","status":"publish","type":"fl_notification","link":"https:\\/\\/www.wpbeaverbuilder.com\\/?fl_notification=welcome-2-1-alpha","title":{"rendered":"Welcome to 2.1 Alpha!"},"content":{"rendered":"<p><a href=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/01\\/thick-beaver.png\\"><img class=\\"size-full wp-image-332142 alignleft\\" src=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/01\\/thick-beaver.png\\" alt=\\"\\" width=\\"80\\" height=\\"80\\" srcset=\\"https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/01\\/thick-beaver.png 80w, https:\\/\\/www.wpbeaverbuilder.com\\/wp-content\\/uploads\\/2018\\/01\\/thick-beaver-45x45.png 45w\\" sizes=\\"(max-width: 80px) 100vw, 80px\\" \\/><\\/a>\\u00a0Hey there! Thank you so much for installing the latest alpha build of\\u00a0Beaver Builder. This is our new notification center! The plan here is to make it easier for us to communicate with\\u00a0<em>you.<\\/em> From here, we can share news about updates and new features, share our blog posts and tutorials, and push out helpful information to everyone using\\u00a0Beaver Builder.<\\/p>\\n<p>During the alpha period, we will be testing the notification center here. We may send out videos, or blogs posts, or different types of content for testing purposes. We''ll do our best to keep them informative and entertaining, but do be aware that, for the most part, these will be &#8220;alpha&#8221; notifications. Thanks, again!<\\/p>\\n","protected":false},"template":"","meta":{"_fl_notification":["https:\\/\\/www.wpbeaverbuilder.com"]},"_links":{"self":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification\\/333965"}],"collection":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/fl_notification"}],"about":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/types\\/fl_notification"}],"wp:attachment":[{"href":"https:\\/\\/www.wpbeaverbuilder.com\\/wp-json\\/wp\\/v2\\/media?parent=333965"}],"curies":[{"name":"wp","href":"https:\\/\\/api.w.org\\/{rel}","templated":true}]}}]";}', 'yes'),
(240, 'fl_themes_subscription_email', '7a62702e62657262666e7a6270656e7a4062736176', 'no'),
(142, 'category_children', 'a:0:{}', 'yes'),
(217, 'can_compress_scripts', '0', 'no'),
(856, 'factory_plugin_activated_bizpanda', '1544030017', 'yes'),
(966, '_transient_timeout_opanda_subscribers_count', '1544030727', 'no'),
(967, '_transient_opanda_subscribers_count', '0', 'no'),
(869, 'factory_wp_content_access_tested', '1', 'yes'),
(870, 'factory_css_js_compression', '1', 'yes'),
(854, 'opanda_default_signin_locker_id', '27', 'yes'),
(804, '_transient_timeout__woocommerce_helper_subscriptions', '1544030767', 'no'),
(805, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(778, '_transient_timeout__woocommerce_helper_updates', '1544072154', 'no'),
(779, '_transient__woocommerce_helper_updates', 'a:4:{s:4:"hash";s:32:"d751713988987e9331980363e24189ce";s:7:"updated";i:1544028954;s:8:"products";a:0:{}s:6:"errors";a:1:{i:0;s:10:"http-error";}}', 'no'),
(785, '_transient_timeout_plugin_slugs', '1544116522', 'no'),
(786, '_transient_plugin_slugs', 'a:21:{i:0;s:19:"akismet/akismet.php";i:1;s:15:"bloom/bloom.php";i:2;s:25:"cookie-bar/cookie-bar.php";i:3;s:29:"divi-builder/divi-builder.php";i:4;s:33:"duplicate-post/duplicate-post.php";i:5;s:45:"enable-media-replace/enable-media-replace.php";i:6;s:43:"export-users-to-csv/export-users-to-csv.php";i:7;s:43:"google-analytics-dashboard-for-wp/gadwp.php";i:8;s:19:"monarch/monarch.php";i:9;s:27:"ninja-forms/ninja-forms.php";i:10;s:36:"ninja-forms-uploads/file-uploads.php";i:11;s:57:"ninja-forms-paypal-express/ninja-forms-paypal-express.php";i:12;s:53:"ninja-forms-table-editor/ninja-forms-table-editor.php";i:13;s:34:"woocommerce-advanced-gift/main.php";i:14;s:31:"sg-cachepress/sg-cachepress.php";i:15;s:47:"sociallocker-next-premium/sociallocker-next.php";i:16;s:27:"woocommerce/woocommerce.php";i:17;s:42:"woocommerce-menu-bar-cart/wp-menu-cart.php";i:18;s:80:"woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php";i:19;s:61:"woo-orders-date-range-filter/woo-orders-date-range-filter.php";i:20;s:41:"yith-woocommerce-product-bundles/init.php";}', 'no'),
(787, '_transient_timeout_wpo_wcpdf_upgrade_notice_2.2.4', '1544115398', 'no'),
(788, '_transient_wpo_wcpdf_upgrade_notice_2.2.4', '', 'no'),
(839, 'onp_default_license_sociallocker-next', 'a:5:{s:8:"Category";s:4:"free";s:5:"Build";s:7:"premium";s:5:"Title";s:21:"OnePress Zero License";s:11:"Description";s:143:"Please, activate the plugin to get started. Enter a key \r\n                                    you received with the plugin into the form below.";s:7:"Expired";i:0;}', 'yes'),
(820, 'et_core_version', '3.17.6', 'yes'),
(822, 'et_divi_builder_plugin', 'a:5:{s:39:"static_css_custom_css_safety_check_done";b:1;s:27:"et_pb_clear_templates_cache";b:1;s:40:"divi_email_provider_credentials_migrated";b:1;s:21:"et_pb_layouts_updated";b:1;s:30:"library_removed_legacy_layouts";b:1;}', 'yes'),
(904, '_site_transient_et_core_version', '3.17.6', 'no'),
(901, '_site_transient_timeout_et_core_path', '1544116466', 'no'),
(902, '_site_transient_et_core_path', '/web/htdocs/www.mbsn.es/home/wp-content/plugins/divi-builder/core', 'no'),
(903, '_site_transient_timeout_et_core_version', '1544116466', 'no'),
(828, 'et_bloom_options', 'a:2:{s:14:"schema_version";i:1;s:10:"db_version";s:3:"1.2";}', 'yes'),
(829, 'et_core_api_email_options', 'a:2:{i:0;b:0;s:8:"accounts";a:0:{}}', 'yes'),
(830, 'widget_bloomwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(953, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1544029962;s:7:"checked";a:21:{s:19:"akismet/akismet.php";s:3:"4.1";s:15:"bloom/bloom.php";s:5:"1.3.6";s:25:"cookie-bar/cookie-bar.php";s:5:"1.8.5";s:29:"divi-builder/divi-builder.php";s:6:"2.17.6";s:33:"duplicate-post/duplicate-post.php";s:5:"3.2.2";s:45:"enable-media-replace/enable-media-replace.php";s:5:"3.2.7";s:43:"export-users-to-csv/export-users-to-csv.php";s:5:"1.1.1";s:43:"google-analytics-dashboard-for-wp/gadwp.php";s:5:"5.3.7";s:19:"monarch/monarch.php";s:5:"1.4.8";s:27:"ninja-forms/ninja-forms.php";s:8:"3.3.19.1";s:36:"ninja-forms-uploads/file-uploads.php";s:6:"3.0.20";s:57:"ninja-forms-paypal-express/ninja-forms-paypal-express.php";s:6:"3.0.14";s:53:"ninja-forms-table-editor/ninja-forms-table-editor.php";s:5:"3.0.4";s:34:"woocommerce-advanced-gift/main.php";s:3:"4.6";s:31:"sg-cachepress/sg-cachepress.php";s:5:"5.0.5";s:47:"sociallocker-next-premium/sociallocker-next.php";s:5:"5.2.6";s:27:"woocommerce/woocommerce.php";s:5:"3.5.2";s:42:"woocommerce-menu-bar-cart/wp-menu-cart.php";s:5:"2.7.3";s:80:"woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php";s:5:"2.2.5";s:61:"woo-orders-date-range-filter/woo-orders-date-range-filter.php";s:5:"2.0.0";s:41:"yith-woocommerce-product-bundles/init.php";s:5:"1.1.9";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:14:{s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"4.1";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.4.1.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:25:"cookie-bar/cookie-bar.php";O:8:"stdClass":9:{s:2:"id";s:24:"w.org/plugins/cookie-bar";s:4:"slug";s:10:"cookie-bar";s:6:"plugin";s:25:"cookie-bar/cookie-bar.php";s:11:"new_version";s:5:"1.8.5";s:3:"url";s:41:"https://wordpress.org/plugins/cookie-bar/";s:7:"package";s:53:"https://downloads.wordpress.org/plugin/cookie-bar.zip";s:5:"icons";a:1:{s:2:"1x";s:63:"https://ps.w.org/cookie-bar/assets/icon-128x128.png?rev=1446263";}s:7:"banners";a:1:{s:2:"1x";s:65:"https://ps.w.org/cookie-bar/assets/banner-772x250.png?rev=1446263";}s:11:"banners_rtl";a:0:{}}s:33:"duplicate-post/duplicate-post.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/duplicate-post";s:4:"slug";s:14:"duplicate-post";s:6:"plugin";s:33:"duplicate-post/duplicate-post.php";s:11:"new_version";s:5:"3.2.2";s:3:"url";s:45:"https://wordpress.org/plugins/duplicate-post/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/duplicate-post.3.2.2.zip";s:5:"icons";a:2:{s:2:"2x";s:67:"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753";s:2:"1x";s:67:"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753";}s:7:"banners";a:1:{s:2:"1x";s:69:"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986";}s:11:"banners_rtl";a:0:{}}s:45:"enable-media-replace/enable-media-replace.php";O:8:"stdClass":9:{s:2:"id";s:34:"w.org/plugins/enable-media-replace";s:4:"slug";s:20:"enable-media-replace";s:6:"plugin";s:45:"enable-media-replace/enable-media-replace.php";s:11:"new_version";s:5:"3.2.7";s:3:"url";s:51:"https://wordpress.org/plugins/enable-media-replace/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/enable-media-replace.zip";s:5:"icons";a:2:{s:2:"2x";s:73:"https://ps.w.org/enable-media-replace/assets/icon-256x256.png?rev=1940728";s:2:"1x";s:73:"https://ps.w.org/enable-media-replace/assets/icon-128x128.png?rev=1940728";}s:7:"banners";a:2:{s:2:"2x";s:76:"https://ps.w.org/enable-media-replace/assets/banner-1544x500.png?rev=1940728";s:2:"1x";s:75:"https://ps.w.org/enable-media-replace/assets/banner-772x250.png?rev=1940728";}s:11:"banners_rtl";a:0:{}}s:43:"export-users-to-csv/export-users-to-csv.php";O:8:"stdClass":9:{s:2:"id";s:33:"w.org/plugins/export-users-to-csv";s:4:"slug";s:19:"export-users-to-csv";s:6:"plugin";s:43:"export-users-to-csv/export-users-to-csv.php";s:11:"new_version";s:5:"1.1.1";s:3:"url";s:50:"https://wordpress.org/plugins/export-users-to-csv/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/export-users-to-csv.1.1.1.zip";s:5:"icons";a:2:{s:2:"2x";s:72:"https://ps.w.org/export-users-to-csv/assets/icon-256x256.jpg?rev=1739282";s:2:"1x";s:72:"https://ps.w.org/export-users-to-csv/assets/icon-128x128.jpg?rev=1739282";}s:7:"banners";a:2:{s:2:"2x";s:75:"https://ps.w.org/export-users-to-csv/assets/banner-1544x500.jpg?rev=1739282";s:2:"1x";s:74:"https://ps.w.org/export-users-to-csv/assets/banner-772x250.jpg?rev=1739282";}s:11:"banners_rtl";a:0:{}}s:43:"google-analytics-dashboard-for-wp/gadwp.php";O:8:"stdClass":9:{s:2:"id";s:47:"w.org/plugins/google-analytics-dashboard-for-wp";s:4:"slug";s:33:"google-analytics-dashboard-for-wp";s:6:"plugin";s:43:"google-analytics-dashboard-for-wp/gadwp.php";s:11:"new_version";s:5:"5.3.7";s:3:"url";s:64:"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/";s:7:"package";s:82:"https://downloads.wordpress.org/plugin/google-analytics-dashboard-for-wp.5.3.7.zip";s:5:"icons";a:2:{s:2:"2x";s:85:"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-256x256.png?rev=970326";s:2:"1x";s:85:"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-128x128.png?rev=970326";}s:7:"banners";a:1:{s:2:"1x";s:88:"https://ps.w.org/google-analytics-dashboard-for-wp/assets/banner-772x250.png?rev=1064664";}s:11:"banners_rtl";a:0:{}}s:27:"ninja-forms/ninja-forms.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/ninja-forms";s:4:"slug";s:11:"ninja-forms";s:6:"plugin";s:27:"ninja-forms/ninja-forms.php";s:11:"new_version";s:8:"3.3.19.1";s:3:"url";s:42:"https://wordpress.org/plugins/ninja-forms/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/ninja-forms.3.3.19.1.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/ninja-forms/assets/icon-256x256.png?rev=1649747";s:2:"1x";s:64:"https://ps.w.org/ninja-forms/assets/icon-128x128.png?rev=1649747";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/ninja-forms/assets/banner-1544x500.png?rev=1649747";s:2:"1x";s:66:"https://ps.w.org/ninja-forms/assets/banner-772x250.png?rev=1649747";}s:11:"banners_rtl";a:0:{}}s:31:"sg-cachepress/sg-cachepress.php";O:8:"stdClass":9:{s:2:"id";s:27:"w.org/plugins/sg-cachepress";s:4:"slug";s:13:"sg-cachepress";s:6:"plugin";s:31:"sg-cachepress/sg-cachepress.php";s:11:"new_version";s:5:"5.0.5";s:3:"url";s:44:"https://wordpress.org/plugins/sg-cachepress/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/sg-cachepress.zip";s:5:"icons";a:2:{s:2:"2x";s:66:"https://ps.w.org/sg-cachepress/assets/icon-256x256.png?rev=1984473";s:2:"1x";s:66:"https://ps.w.org/sg-cachepress/assets/icon-128x128.png?rev=1984473";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/sg-cachepress/assets/banner-1544x500.png?rev=1984472";s:2:"1x";s:68:"https://ps.w.org/sg-cachepress/assets/banner-772x250.png?rev=1984472";}s:11:"banners_rtl";a:0:{}}s:47:"sociallocker-next-premium/sociallocker-next.php";O:8:"stdClass":9:{s:2:"id";s:27:"w.org/plugins/social-locker";s:4:"slug";s:13:"social-locker";s:6:"plugin";s:47:"sociallocker-next-premium/sociallocker-next.php";s:11:"new_version";s:5:"5.2.6";s:3:"url";s:44:"https://wordpress.org/plugins/social-locker/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/social-locker.zip";s:5:"icons";a:2:{s:2:"2x";s:65:"https://ps.w.org/social-locker/assets/icon-256x256.png?rev=984149";s:2:"1x";s:65:"https://ps.w.org/social-locker/assets/icon-128x128.png?rev=984149";}s:7:"banners";a:1:{s:2:"1x";s:68:"https://ps.w.org/social-locker/assets/banner-772x250.png?rev=1636213";}s:11:"banners_rtl";a:0:{}}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/woocommerce";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:5:"3.5.2";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/woocommerce.3.5.2.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831";s:2:"1x";s:64:"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184";s:2:"1x";s:66:"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184";}s:11:"banners_rtl";a:0:{}}s:42:"woocommerce-menu-bar-cart/wp-menu-cart.php";O:8:"stdClass":9:{s:2:"id";s:39:"w.org/plugins/woocommerce-menu-bar-cart";s:4:"slug";s:25:"woocommerce-menu-bar-cart";s:6:"plugin";s:42:"woocommerce-menu-bar-cart/wp-menu-cart.php";s:11:"new_version";s:5:"2.7.3";s:3:"url";s:56:"https://wordpress.org/plugins/woocommerce-menu-bar-cart/";s:7:"package";s:74:"https://downloads.wordpress.org/plugin/woocommerce-menu-bar-cart.2.7.3.zip";s:5:"icons";a:2:{s:2:"2x";s:77:"https://ps.w.org/woocommerce-menu-bar-cart/assets/icon-256x256.jpg?rev=987902";s:2:"1x";s:77:"https://ps.w.org/woocommerce-menu-bar-cart/assets/icon-128x128.jpg?rev=987902";}s:7:"banners";a:1:{s:2:"1x";s:79:"https://ps.w.org/woocommerce-menu-bar-cart/assets/banner-772x250.jpg?rev=687839";}s:11:"banners_rtl";a:0:{}}s:80:"woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php";O:8:"stdClass":9:{s:2:"id";s:52:"w.org/plugins/woocommerce-pdf-invoices-packing-slips";s:4:"slug";s:38:"woocommerce-pdf-invoices-packing-slips";s:6:"plugin";s:80:"woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php";s:11:"new_version";s:5:"2.2.5";s:3:"url";s:69:"https://wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/";s:7:"package";s:87:"https://downloads.wordpress.org/plugin/woocommerce-pdf-invoices-packing-slips.2.2.5.zip";s:5:"icons";a:2:{s:2:"2x";s:90:"https://ps.w.org/woocommerce-pdf-invoices-packing-slips/assets/icon-256x256.png?rev=983013";s:2:"1x";s:90:"https://ps.w.org/woocommerce-pdf-invoices-packing-slips/assets/icon-128x128.png?rev=983013";}s:7:"banners";a:1:{s:2:"1x";s:92:"https://ps.w.org/woocommerce-pdf-invoices-packing-slips/assets/banner-772x250.png?rev=840317";}s:11:"banners_rtl";a:0:{}}s:61:"woo-orders-date-range-filter/woo-orders-date-range-filter.php";O:8:"stdClass":9:{s:2:"id";s:42:"w.org/plugins/woo-orders-date-range-filter";s:4:"slug";s:28:"woo-orders-date-range-filter";s:6:"plugin";s:61:"woo-orders-date-range-filter/woo-orders-date-range-filter.php";s:11:"new_version";s:5:"2.0.0";s:3:"url";s:59:"https://wordpress.org/plugins/woo-orders-date-range-filter/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/woo-orders-date-range-filter.zip";s:5:"icons";a:2:{s:2:"2x";s:81:"https://ps.w.org/woo-orders-date-range-filter/assets/icon-256x256.png?rev=1636762";s:2:"1x";s:81:"https://ps.w.org/woo-orders-date-range-filter/assets/icon-128x128.png?rev=1636762";}s:7:"banners";a:1:{s:2:"1x";s:83:"https://ps.w.org/woo-orders-date-range-filter/assets/banner-772x250.png?rev=1636762";}s:11:"banners_rtl";a:0:{}}s:41:"yith-woocommerce-product-bundles/init.php";O:8:"stdClass":9:{s:2:"id";s:46:"w.org/plugins/yith-woocommerce-product-bundles";s:4:"slug";s:32:"yith-woocommerce-product-bundles";s:6:"plugin";s:41:"yith-woocommerce-product-bundles/init.php";s:11:"new_version";s:5:"1.1.9";s:3:"url";s:63:"https://wordpress.org/plugins/yith-woocommerce-product-bundles/";s:7:"package";s:75:"https://downloads.wordpress.org/plugin/yith-woocommerce-product-bundles.zip";s:5:"icons";a:1:{s:2:"1x";s:85:"https://ps.w.org/yith-woocommerce-product-bundles/assets/icon-128x128.jpg?rev=1202320";}s:7:"banners";a:2:{s:2:"2x";s:88:"https://ps.w.org/yith-woocommerce-product-bundles/assets/banner-1544x500.jpg?rev=1202320";s:2:"1x";s:87:"https://ps.w.org/yith-woocommerce-product-bundles/assets/banner-772x250.jpg?rev=1202342";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(840, 'factory_plugin_activated_sociallocker-next', '1544030017', 'yes'),
(841, 'factory_plugin_versions', 'a:2:{s:17:"sociallocker-next";s:13:"premium-5.2.6";s:8:"bizpanda";s:6:"-1.3.5";}', 'yes'),
(842, 'opanda_facebook_appid', '117100935120196', 'yes'),
(843, 'opanda_facebook_version', 'v3.0', 'yes'),
(844, 'opanda_lang', 'en_US', 'yes'),
(845, 'opanda_short_lang', 'en', 'yes'),
(846, 'opanda_tracking', 'true', 'yes'),
(847, 'opanda_just_social_buttons', 'false', 'yes'),
(848, 'opanda_subscription_service', 'database', 'yes'),
(849, 'opanda_terms_enabled', '1', 'yes'),
(850, 'opanda_privacy_enabled', '1', 'yes'),
(851, 'opanda_terms_use_pages', '0', 'yes'),
(852, 'opanda_terms_of_use_text', '<h2>Terms of Use</h2>\r\n<p><em>This Terms of Use explains the operation principle of the Content Lockers placed on this website.</em></p>\r\n<p>On this website, you can encounter the Content Lockers which may ask<br /> you to sign in, subscribe, enter your name or perform other actions to get access to the locked content.</p>\r\n<h4>Using your email address</h4>\r\n<p>When you enter your email or sign in through social networks, you agree to that your email address will be added to the subscription list for sending target news and special offers.</p>\r\n<p>You can unsubscribe at any time by clicking on the link at the end of any of emails received from us.</p>\r\n<h4>Social Apps &amp; Permissions</h4>\r\n<p>When you sign in through social networks, the Content Locker may ask you to grant permissions to read or perform some social actions.</p>\r\n<p>The Content Locker retrieves only the following information (according the Privacy Policy of this website):</p>\r\n<ul>\r\n<li>Person name</li>\r\n<li>Email address</li>\r\n</ul>\r\n<p>Content Locker never collects other data and never publish anything in social networks from your behalf without your permissions.</p>\r\n<p>After unlocking the content the Content Locker removes all the access tokens received from you and never uses them again.</p>\r\n<p>If there are any questions regarding this Terms of Use you may contact us.</p>', 'yes'),
(853, 'opanda_privacy_policy_text', '<h2>Privacy Policy</h2>\r\n<p><em>This privacy policy has been compiled to better serve those who are concerned with how their ''Personally identifiable information'' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context.</em></p>\r\n<p>Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>\r\n<h4>What personal information do we collect from the people that visit our blog, website or app?</h4>\r\n<p>When using our site, as appropriate, you may be asked to enter your name, email address or other details to help you with your experience.</p>\r\n<h4>How do we use your information?</h4>\r\n<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, unlock the content, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>\r\n<ul>\r\n<li>To personalize user''s experience and to allow us to deliver the type of content and product offerings in which you are most interested</li>\r\n<li>To improve our website in order to better serve you.</li>\r\n<li>To allow us to better service you in responding to your customer service requests.</li>\r\n<li>To administer a contest, promotion, survey or other site feature.</li>\r\n<li>To send periodic emails regarding your order or other products and services.</li>\r\n</ul>\r\n<h4>Do we use ''cookies''?</h4>\r\n<p>Yes. Cookies are small files that a site or its service provider transfers to your computer''s hard drive through your Web browser (if you allow) that enables the site''s or service provider''s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>\r\n<p>We use cookies to:</p>\r\n<ul>\r\n<li>Understand and save user''s preferences for future visits.</li>\r\n<li>Memorize the users who have unlocked access to premium content.</li>\r\n</ul>\r\n<p>If you disable cookies off, some features will be disabled. It will affect the users experience and some of our services will not function properly.</p>\r\n<h4>Third Party Disclosure</h4>\r\n<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide you with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others'' rights, property, or safety.</p>\r\n<p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>\r\n<h4>Third party links</h4>\r\n<p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>\r\n<p>If there are any questions regarding this privacy policy you may contact us.</p>', 'yes'),
(663, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:"post";i:1;s:4:"page";}', 'yes'),
(664, 'duplicate_post_show_row', '1', 'yes'),
(665, 'duplicate_post_show_adminbar', '1', 'yes'),
(666, 'duplicate_post_show_submitbox', '1', 'yes'),
(667, 'duplicate_post_show_bulkactions', '1', 'yes'),
(668, 'duplicate_post_version', '3.2.2', 'yes'),
(669, 'duplicate_post_show_notice', '1', 'no'),
(751, '_transient_timeout_external_ip_address_66.249.81.226', '1544630267', 'no'),
(752, '_transient_external_ip_address_66.249.81.226', '89.46.107.98', 'no'),
(770, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1544039636', 'no'),
(771, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:"stdClass":100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";i:4527;}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";i:3155;}s:4:"post";a:3:{s:4:"name";s:4:"post";s:4:"slug";s:4:"post";s:5:"count";i:2602;}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";i:2462;}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";i:1891;}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";i:1698;}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";i:1690;}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";i:1460;}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";i:1412;}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";i:1411;}s:8:"facebook";a:3:{s:4:"name";s:8:"facebook";s:4:"slug";s:8:"facebook";s:5:"count";i:1410;}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";i:1343;}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";i:1284;}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";i:1265;}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";i:1124;}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";i:1081;}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";i:1065;}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";i:1044;}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";i:954;}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";i:906;}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";i:838;}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";i:823;}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";i:813;}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";i:744;}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";i:716;}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";i:706;}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";i:700;}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";i:698;}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";i:690;}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";i:673;}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";i:671;}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";i:670;}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";i:652;}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";i:647;}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";i:629;}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";i:629;}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";i:620;}s:4:"ajax";a:3:{s:4:"name";s:4:"ajax";s:4:"slug";s:4:"ajax";s:5:"count";i:611;}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";i:608;}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";i:602;}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";i:574;}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";i:558;}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";i:550;}s:3:"css";a:3:{s:4:"name";s:3:"css";s:4:"slug";s:3:"css";s:5:"count";i:549;}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";i:540;}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";i:539;}s:5:"share";a:3:{s:4:"name";s:5:"share";s:4:"slug";s:5:"share";s:5:"count";i:528;}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";i:520;}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";i:519;}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";i:514;}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";i:512;}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";i:505;}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";i:496;}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";i:490;}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";i:490;}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";i:482;}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";i:468;}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";i:465;}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";i:461;}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";i:454;}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";i:450;}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";i:446;}s:7:"payment";a:3:{s:4:"name";s:7:"payment";s:4:"slug";s:7:"payment";s:5:"count";i:441;}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";i:427;}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";i:427;}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";i:420;}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";i:418;}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";i:417;}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";i:412;}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";i:398;}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";i:390;}s:15:"payment-gateway";a:3:{s:4:"name";s:15:"payment gateway";s:4:"slug";s:15:"payment-gateway";s:5:"count";i:389;}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";i:380;}s:4:"chat";a:3:{s:4:"name";s:4:"chat";s:4:"slug";s:4:"chat";s:5:"count";i:372;}s:4:"news";a:3:{s:4:"name";s:4:"news";s:4:"slug";s:4:"news";s:5:"count";i:372;}s:5:"popup";a:3:{s:4:"name";s:5:"popup";s:4:"slug";s:5:"popup";s:5:"count";i:372;}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";i:371;}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";i:370;}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";i:356;}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";i:355;}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";i:355;}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";i:353;}s:4:"code";a:3:{s:4:"name";s:4:"code";s:4:"slug";s:4:"code";s:5:"count";i:346;}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";i:343;}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";i:341;}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";i:338;}s:5:"forms";a:3:{s:4:"name";s:5:"forms";s:4:"slug";s:5:"forms";s:5:"count";i:336;}s:8:"redirect";a:3:{s:4:"name";s:8:"redirect";s:4:"slug";s:8:"redirect";s:5:"count";i:333;}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";i:316;}s:11:"performance";a:3:{s:4:"name";s:11:"performance";s:4:"slug";s:11:"performance";s:5:"count";i:315;}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";i:314;}s:16:"custom-post-type";a:3:{s:4:"name";s:16:"custom post type";s:4:"slug";s:16:"custom-post-type";s:5:"count";i:311;}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";i:311;}s:14:"contact-form-7";a:3:{s:4:"name";s:14:"contact form 7";s:4:"slug";s:14:"contact-form-7";s:5:"count";i:307;}s:16:"google-analytics";a:3:{s:4:"name";s:16:"google analytics";s:4:"slug";s:16:"google-analytics";s:5:"count";i:306;}s:8:"tracking";a:3:{s:4:"name";s:8:"tracking";s:4:"slug";s:8:"tracking";s:5:"count";i:305;}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";i:305;}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";i:304;}s:7:"adsense";a:3:{s:4:"name";s:7:"adsense";s:4:"slug";s:7:"adsense";s:5:"count";i:303;}s:6:"author";a:3:{s:4:"name";s:6:"author";s:4:"slug";s:6:"author";s:5:"count";i:299;}}', 'no'),
(651, 'duplicate_post_copycontent', '1', 'yes'),
(652, 'duplicate_post_copythumbnail', '1', 'yes'),
(653, 'duplicate_post_copytemplate', '1', 'yes'),
(654, 'duplicate_post_copyformat', '1', 'yes'),
(655, 'duplicate_post_copyauthor', '0', 'yes'),
(656, 'duplicate_post_copypassword', '0', 'yes'),
(657, 'duplicate_post_copyattachments', '0', 'yes'),
(658, 'duplicate_post_copychildren', '0', 'yes'),
(659, 'duplicate_post_copycomments', '0', 'yes'),
(660, 'duplicate_post_copymenuorder', '1', 'yes'),
(661, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(662, 'duplicate_post_blacklist', '', 'yes'),
(864, 'onp_gate_token', '8b4224068a41c5d37f5e2d54f3995089', 'yes'),
(865, 'onp_gate_expired', '1544033619', 'yes'),
(866, 'onp_version_check_sociallocker-next', 'a:10:{s:2:"Id";i:483;s:5:"Build";s:7:"premium";s:7:"Version";s:5:"5.2.6";s:10:"Registered";i:1531136144;s:7:"Checked";i:1544030019;s:10:"DetailsURL";s:54:"http://api.byonepress.com/1.1/GetDetails?versionId=483";s:10:"PackageURL";s:54:"http://api.byonepress.com/1.1/GetPackage?versionId=483";s:13:"DeleteLicense";b:0;s:11:"KeyNotBound";b:0;s:10:"SiteSecret";N;}', 'yes'),
(868, 'onp_last_check_sociallocker-next', '1544030019', 'yes'),
(872, 'et_pb_builder_options', 'a:2:{i:0;b:0;s:35:"email_provider_credentials_migrated";b:1;}', 'yes'),
(873, 'opanda_robust_key', '0f7a81', 'yes');
INSERT INTO `lab_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(875, 'pw_gift_options', 'a:38:{s:10:"show_popup";s:3:"yes";s:22:"multiselect_gift_count";s:1:"1";s:23:"multiselect_cart_amount";s:1:"0";s:8:"auto_add";s:2:"no";s:11:"multiselect";s:2:"no";s:14:"predifine_gift";s:2:"no";s:19:"popup_pw_item_width";s:3:"200";s:21:"popup_pw_item_marrgin";s:2:"10";s:24:"popup_pw_show_pagination";s:5:"false";s:21:"popup_pw_show_control";s:4:"true";s:22:"popup_pw_item_per_view";s:1:"4";s:23:"popup_pw_item_per_slide";s:1:"1";s:20:"popup_pw_slide_speed";s:4:"4000";s:18:"popup_pw_auto_play";s:4:"true";s:20:"hide_popup_in_mobile";s:4:"true";s:13:"pw_item_width";s:3:"200";s:15:"pw_item_marrgin";s:2:"10";s:18:"pw_show_pagination";s:5:"false";s:15:"pw_show_control";s:4:"true";s:16:"pw_item_per_view";s:1:"4";s:17:"pw_item_per_slide";s:1:"1";s:14:"pw_slide_speed";s:4:"4000";s:12:"pw_auto_play";s:4:"true";s:12:"pw_slide_rtl";s:5:"false";s:15:"number_per_page";s:1:"6";s:11:"popup_title";s:9:"Our gifts";s:10:"cart_title";s:9:"Our gifts";s:4:"Hour";s:4:"Hour";s:4:"free";s:4:"free";s:7:"Minutes";s:7:"Minutes";s:7:"Seconds";s:7:"Seconds";s:14:"view_cart_gift";s:4:"grid";s:15:"desktop_columns";s:11:"wg-col-md-3";s:14:"tablet_columns";s:12:"wg-col-sm-12";s:14:"mobile_columns";s:12:"wg-col-xs-12";s:23:"hide_gifts_after_select";s:2:"no";s:8:"add_gift";s:8:"ADD GIFT";s:18:"txt_single_product";s:15:"product gift(s)";}', 'yes'),
(876, 'pw_wc_advanced_gift', 'install', 'yes'),
(877, 'pw_gift_version', '4.6', 'yes'),
(893, '_transient_timeout_nf_ppe_tls_ver', '1544116451', 'no'),
(887, 'nf_tel_collate', '1', 'no'),
(894, '_transient_nf_ppe_tls_ver', 'yes', 'no'),
(907, 'siteground_optimizer_default_enable_cache', '0', 'no'),
(908, 'siteground_optimizer_default_autoflush_cache', '0', 'no'),
(909, 'siteground_optimizer_supercacher_permissions', '1', 'no'),
(910, 'sg_cachepress', 'a:8:{s:12:"enable_cache";i:1;s:15:"autoflush_cache";i:1;s:16:"enable_memcached";i:0;s:11:"show_notice";i:0;s:8:"is_nginx";i:0;s:13:"checked_nginx";i:0;s:9:"first_run";i:0;s:9:"last_fail";i:0;}', 'yes'),
(911, 'siteground_optimizer_enable_cache', '1', 'yes'),
(912, 'siteground_optimizer_autoflush_cache', '1', 'yes'),
(913, 'siteground_optimizer_enable_memcached', '0', 'yes'),
(914, 'siteground_optimizer_show_notice', '0', 'yes'),
(915, 'siteground_optimizer_is_nginx', '0', 'yes'),
(916, 'siteground_optimizer_checked_nginx', '0', 'yes'),
(917, 'siteground_optimizer_first_run', '0', 'yes'),
(918, 'siteground_optimizer_last_fail', '0', 'yes'),
(919, 'siteground_optimizer_ssl_enabled', '0', 'yes'),
(920, 'siteground_optimizer_optimize_html', '0', 'yes'),
(921, 'siteground_optimizer_optimize_javascript', '0', 'yes'),
(922, 'siteground_optimizer_optimize_css', '0', 'yes'),
(923, 'siteground_optimizer_remove_query_strings', '0', 'yes'),
(924, 'siteground_optimizer_disable_emojis', '0', 'yes'),
(925, 'siteground_optimizer_optimize_images', '0', 'yes'),
(927, 'siteground_optimizer_version', '5.0.5', 'yes'),
(928, 'siteground_optimizer_phpcompat_status', '1', 'yes'),
(929, 'siteground_optimizer_phpcompat_progress', '0', 'yes'),
(930, 'siteground_optimizer_phpcompat_is_compatible', '0', 'yes'),
(931, 'siteground_optimizer_phpcompat_result', 'a:0:{}', 'yes'),
(932, 'siteground_optimizer_image_optimization_completed', '1', 'no'),
(231, 'recently_activated', 'a:1:{s:24:"bb-plugin/fl-builder.php";i:1543779330;}', 'yes'),
(239, '_fl_builder_version', '2.1.6.3', 'no'),
(241, 'widget_akismet_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(246, '_fl_builder_update_info', 'a:2:{s:4:"from";s:7:"2.1.4.2";s:2:"to";s:7:"2.1.6.3";}', 'no'),
(256, 'ninja_forms_data_is_clean', 'true', 'yes'),
(257, 'ninja_forms_version', '3.3.19.1', 'yes'),
(258, 'ninja_forms_db_version', '1.1', 'no'),
(259, 'ninja_forms_settings', 'a:8:{s:11:"date_format";s:5:"m/d/Y";s:8:"currency";s:3:"USD";s:18:"recaptcha_site_key";s:0:"";s:20:"recaptcha_secret_key";s:0:"";s:14:"recaptcha_lang";s:0:"";s:19:"delete_on_uninstall";i:0;s:21:"disable_admin_notices";i:0;s:21:"uploads_table_version";s:6:"3.0.10";}', 'yes'),
(260, 'wp_nf_update_fields_batch_0678dee3f83216bbafcc7a13b4a4aed1', 'a:4:{i:0;a:2:{s:2:"id";i:1;s:8:"settings";a:70:{s:5:"label";s:4:"Name";s:3:"key";s:4:"name";s:9:"parent_id";i:1;s:4:"type";s:7:"textbox";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"1";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"input_limit";s:0:"";s:16:"input_limit_type";s:10:"characters";s:15:"input_limit_msg";s:17:"Character(s) left";s:10:"manual_key";s:0:"";s:13:"disable_input";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"disable_browser_autocomplete";s:0:"";s:4:"mask";s:0:"";s:11:"custom_mask";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3277";}}i:1;a:2:{s:2:"id";i:2;s:8:"settings";a:62:{s:5:"label";s:5:"Email";s:3:"key";s:5:"email";s:9:"parent_id";i:1;s:4:"type";s:5:"email";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"2";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3281";}}i:2;a:2:{s:2:"id";i:3;s:8:"settings";a:71:{s:5:"label";s:7:"Message";s:3:"key";s:7:"message";s:9:"parent_id";i:1;s:4:"type";s:8:"textarea";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:9:"label_pos";s:5:"above";s:8:"required";s:1:"1";s:5:"order";s:1:"3";s:11:"placeholder";s:0:"";s:7:"default";s:0:"";s:13:"wrapper_class";s:0:"";s:13:"element_class";s:0:"";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:11:"input_limit";s:0:"";s:16:"input_limit_type";s:10:"characters";s:15:"input_limit_msg";s:17:"Character(s) left";s:10:"manual_key";s:0:"";s:13:"disable_input";s:0:"";s:11:"admin_label";s:0:"";s:9:"help_text";s:0:"";s:9:"desc_text";s:0:"";s:28:"disable_browser_autocomplete";s:0:"";s:12:"textarea_rte";s:0:"";s:18:"disable_rte_mobile";s:0:"";s:14:"textarea_media";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3284";}}i:3;a:2:{s:2:"id";i:4;s:8:"settings";a:69:{s:5:"label";s:6:"Submit";s:3:"key";s:6:"submit";s:9:"parent_id";i:1;s:4:"type";s:6:"submit";s:10:"created_at";s:19:"2018-11-25 09:42:22";s:16:"processing_label";s:10:"Processing";s:5:"order";s:1:"5";s:10:"objectType";s:5:"Field";s:12:"objectDomain";s:6:"fields";s:10:"editActive";s:0:"";s:15:"container_class";s:0:"";s:13:"element_class";s:0:"";s:28:"wrap_styles_background-color";s:0:"";s:18:"wrap_styles_border";s:0:"";s:24:"wrap_styles_border-style";s:0:"";s:24:"wrap_styles_border-color";s:0:"";s:17:"wrap_styles_color";s:0:"";s:18:"wrap_styles_height";s:0:"";s:17:"wrap_styles_width";s:0:"";s:21:"wrap_styles_font-size";s:0:"";s:18:"wrap_styles_margin";s:0:"";s:19:"wrap_styles_padding";s:0:"";s:19:"wrap_styles_display";s:0:"";s:17:"wrap_styles_float";s:0:"";s:29:"wrap_styles_show_advanced_css";s:1:"0";s:20:"wrap_styles_advanced";s:0:"";s:29:"label_styles_background-color";s:0:"";s:19:"label_styles_border";s:0:"";s:25:"label_styles_border-style";s:0:"";s:25:"label_styles_border-color";s:0:"";s:18:"label_styles_color";s:0:"";s:19:"label_styles_height";s:0:"";s:18:"label_styles_width";s:0:"";s:22:"label_styles_font-size";s:0:"";s:19:"label_styles_margin";s:0:"";s:20:"label_styles_padding";s:0:"";s:20:"label_styles_display";s:0:"";s:18:"label_styles_float";s:0:"";s:30:"label_styles_show_advanced_css";s:1:"0";s:21:"label_styles_advanced";s:0:"";s:31:"element_styles_background-color";s:0:"";s:21:"element_styles_border";s:0:"";s:27:"element_styles_border-style";s:0:"";s:27:"element_styles_border-color";s:0:"";s:20:"element_styles_color";s:0:"";s:21:"element_styles_height";s:0:"";s:20:"element_styles_width";s:0:"";s:24:"element_styles_font-size";s:0:"";s:21:"element_styles_margin";s:0:"";s:22:"element_styles_padding";s:0:"";s:22:"element_styles_display";s:0:"";s:20:"element_styles_float";s:0:"";s:32:"element_styles_show_advanced_css";s:1:"0";s:23:"element_styles_advanced";s:0:"";s:44:"submit_element_hover_styles_background-color";s:0:"";s:34:"submit_element_hover_styles_border";s:0:"";s:40:"submit_element_hover_styles_border-style";s:0:"";s:40:"submit_element_hover_styles_border-color";s:0:"";s:33:"submit_element_hover_styles_color";s:0:"";s:34:"submit_element_hover_styles_height";s:0:"";s:33:"submit_element_hover_styles_width";s:0:"";s:37:"submit_element_hover_styles_font-size";s:0:"";s:34:"submit_element_hover_styles_margin";s:0:"";s:35:"submit_element_hover_styles_padding";s:0:"";s:35:"submit_element_hover_styles_display";s:0:"";s:33:"submit_element_hover_styles_float";s:0:"";s:45:"submit_element_hover_styles_show_advanced_css";s:1:"0";s:36:"submit_element_hover_styles_advanced";s:0:"";s:7:"cellcid";s:5:"c3287";}}}', 'no'),
(251, 'gadwp_options', '{"client_id":"","client_secret":"","access_front":["administrator"],"access_back":["administrator"],"tableid_jail":"","theme_color":"#1e73be","switch_profile":0,"tracking_type":"universal","ga_anonymize_ip":0,"user_api":0,"ga_event_tracking":0,"ga_event_downloads":"zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*","track_exclude":[],"ga_target_geomap":"","ga_realtime_pages":10,"token":"","ga_profiles_list":[],"ga_enhanced_links":0,"ga_remarketing":0,"network_mode":0,"ga_speed_samplerate":1,"ga_user_samplerate":100,"ga_event_bouncerate":0,"ga_crossdomain_tracking":0,"ga_crossdomain_list":"","ga_author_dimindex":0,"ga_category_dimindex":0,"ga_tag_dimindex":0,"ga_user_dimindex":0,"ga_pubyear_dimindex":0,"ga_pubyearmonth_dimindex":0,"ga_aff_tracking":0,"ga_event_affiliates":"\\/out\\/","automatic_updates_minorversion":1,"backend_item_reports":1,"backend_realtime_report":0,"frontend_item_reports":0,"dashboard_widget":1,"api_backoff":0,"ga_cookiedomain":"","ga_cookiename":"","ga_cookieexpires":"","pagetitle_404":"Page Not Found","maps_api_key":"","tm_author_var":0,"tm_category_var":0,"tm_tag_var":0,"tm_user_var":0,"tm_pubyear_var":0,"tm_pubyearmonth_var":0,"web_containerid":"","amp_containerid":"","amp_tracking_tagmanager":0,"amp_tracking_analytics":0,"amp_tracking_clientidapi":0,"trackingcode_infooter":0,"trackingevents_infooter":0,"ecommerce_mode":"disabled","ga_formsubmit_tracking":0,"optimize_tracking":0,"optimize_containerid":"","optimize_pagehiding":0,"superadmin_tracking":0,"ga_pagescrolldepth_tracking":0,"tm_pagescrolldepth_tracking":0,"ga_event_precision":0,"ga_force_ssl":0,"with_endpoint":1,"ga_optout":0,"ga_dnt_optout":0,"tm_optout":0,"tm_dnt_optout":0,"ga_with_gtag":0,"usage_tracking":0,"hide_am_notices":0,"network_hide_am_notices":0,"ga_enhanced_excludesa":0,"ga_hash_tracking":0}', 'yes'),
(252, 'widget_gadwp-frontwidget-report', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(253, '_amn_exact-metrics_last_checked', '1543968000', 'yes'),
(286, 'woocommerce_default_country', 'GB', 'yes'),
(287, 'woocommerce_store_postcode', '', 'yes'),
(288, 'woocommerce_allowed_countries', 'all', 'yes'),
(289, 'woocommerce_all_except_countries', '', 'yes'),
(290, 'woocommerce_specific_allowed_countries', '', 'yes'),
(291, 'woocommerce_ship_to_countries', '', 'yes'),
(292, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(293, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(294, 'woocommerce_calc_taxes', 'no', 'yes'),
(295, 'woocommerce_enable_coupons', 'yes', 'yes'),
(296, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(297, 'woocommerce_currency', 'GBP', 'yes'),
(298, 'woocommerce_currency_pos', 'left', 'yes'),
(299, 'woocommerce_price_thousand_sep', ',', 'yes'),
(300, 'woocommerce_price_decimal_sep', '.', 'yes'),
(301, 'woocommerce_price_num_decimals', '2', 'yes'),
(302, 'woocommerce_shop_page_id', '', 'yes'),
(303, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(304, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(305, 'woocommerce_placeholder_image', '', 'yes'),
(306, 'woocommerce_weight_unit', 'kg', 'yes'),
(307, 'woocommerce_dimension_unit', 'cm', 'yes'),
(308, 'woocommerce_enable_reviews', 'yes', 'yes'),
(309, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(310, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(311, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(312, 'woocommerce_review_rating_required', 'yes', 'no'),
(313, 'woocommerce_manage_stock', 'yes', 'yes'),
(314, 'woocommerce_hold_stock_minutes', '60', 'no'),
(315, 'woocommerce_notify_low_stock', 'yes', 'no'),
(316, 'woocommerce_notify_no_stock', 'yes', 'no'),
(317, 'woocommerce_stock_email_recipient', 'antonio@maisto.info', 'no'),
(318, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(319, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(320, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(321, 'woocommerce_stock_format', '', 'yes'),
(322, 'woocommerce_file_download_method', 'force', 'no'),
(323, 'woocommerce_downloads_require_login', 'no', 'no'),
(324, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(325, 'woocommerce_prices_include_tax', 'no', 'yes'),
(326, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(327, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(328, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(263, 'nf_admin_notice', 'a:1:{s:16:"one_week_support";a:2:{s:5:"start";s:9:"12/2/2018";s:3:"int";i:7;}}', 'yes'),
(262, 'widget_ninja_forms_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(283, 'woocommerce_store_address', '', 'yes'),
(284, 'woocommerce_store_address_2', '', 'yes'),
(285, 'woocommerce_store_city', '', 'yes'),
(265, 'ninja_forms_oauth_client_secret', 'ImcntACsJqM31f5pCJ3HmpkkXF3H704T8Zb9wZNE', 'yes'),
(266, 'ninja_forms_optin_reported', '1', 'yes'),
(267, 'ninja_forms_memberships_feed', '<div class="widget widget-memberships">\r\n<div class="pricing-container">\r\n<div class="pricing-block widget">\r\n<div class="pricing-header">\r\n<div class="pricing-title">Agency</div>\r\n<div class="pricing-price">$499</div>\r\n<div class="pricing-savings">Save $2000+</div>\r\n<div class="pricing-cta"><a class="nf-button primary" href="https://ninjaforms.com/checkout/?edd_action=add_to_cart&download_id=203757&utm_medium=plugin&utm_source=plugin-dashboard&utm_campaign=Ninja+Forms+Memberships&utm_content=Agency+Membership" target="_blank" rel="noopener">Buy Now</a></div>\r\n</div>\r\n<div class="pricing-body">\r\n<div><i class="fa fa-users" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Unlimited Sites</span>\r\n<span>Updates &amp; Support</span></div>\r\n<div><i class="fa fa-rocket" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Fastest Support</span>\r\n<span>1 Year of Updates &amp; Support</span></div>\r\n<div><i class="fa fa-trophy" aria-hidden="true"></i>\r\n<span class="pricing-body-title">All add-ons included!</span>\r\n<ul>\r\n 	<li>Builder PRO Pack</li>\r\n 	<li>PDF Form Submissions</li>\r\n 	<li>Zapier</li>\r\n 	<li>Newsletters (MailChimp, etc.)</li>\r\n 	<li>CRMs (Saleforce, etc.)</li>\r\n 	<li>Payments (PayPal, etc.)</li>\r\n 	<li>And so much more...</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="pricing-block widget highlight">\r\n\r\n<!--\r\n<div class="callout">\r\n                    Most Popular</div>\r\n-->\r\n<div class="pricing-header">\r\n<div class="pricing-title">Professional</div>\r\n<div class="pricing-price">$199</div>\r\n<div class="pricing-savings">Save $197</div>\r\n<div class="pricing-cta"><a class="nf-button primary" href="https://ninjaforms.com/checkout/?edd_action=add_to_cart&download_id=471355&utm_medium=plugin&utm_source=plugin-dashboard&utm_campaign=Ninja+Forms+Memberships&utm_content=Professional+Membership" target="_blank" rel="noopener">Buy Now</a></div>\r\n</div>\r\n<div class="pricing-body">\r\n<div><i class="fa fa-users" aria-hidden="true"></i>\r\n<span class="pricing-body-title">20 Sites</span>\r\n<span>Updates &amp; Support</span></div>\r\n<div><i class="fa fa-plane" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Faster Support</span></div>\r\n<div><i class="fa fa-wrench" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Builder Pro Pack</span>\r\n<span>Layout &amp; Styles, Multi-Part Forms, Conditional Logic, File Uploads</span></div>\r\n<div><i class="fa fa-percent" aria-hidden="true"></i>\r\n<span>Plus <strong>40% off</strong>\r\nAdditional Add-Ons</span></div>\r\n</div>\r\n</div>\r\n<div class="pricing-block widget">\r\n<div class="pricing-header">\r\n<div class="pricing-title">Personal</div>\r\n<div class="pricing-price">$99</div>\r\n<div class="pricing-savings">Save $57</div>\r\n<div class="pricing-cta"><a class="nf-button primary" href="https://ninjaforms.com/checkout/?edd_action=add_to_cart&download_id=471356&utm_medium=plugin&utm_source=plugin-dashboard&utm_campaign=Ninja+Forms+Memberships&utm_content=Personal+Membership" target="_blank" rel="noopener">Buy Now</a></div>\r\n</div>\r\n<div class="pricing-body">\r\n<div><i class="fa fa-user" aria-hidden="true"></i>\r\n<span class="pricing-body-title">1 Site</span>\r\n<span>Updates &amp; Support</span></div>\r\n<div><i class="fa fa-car" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Fast Support</span></div>\r\n<div><i class="fa fa-wrench" aria-hidden="true"></i>\r\n<span class="pricing-body-title">Builder Pro Pack</span>\r\n<span>Layout &amp; Styles, Multi-Part Forms, Conditional Logic, File Uploads</span></div>\r\n<div><i class="fa fa-percent" aria-hidden="true"></i>\r\n<span>Plus <strong>20% off</strong>\r\nAdditional Add-Ons</span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="widget widget-plan-notice">\r\n<p class="widget-title">That sounds amazing! What else comes with Ninja Forms?</p>\r\n<a class="nf-button primary feature-list-link" href="https://ninjaforms.com/features/?utm_medium=plugin&utm_source=plugin-dashboard&utm_campaign=Ninja+Forms+Memberships&utm_content=Features" target="_blank" rel="noopener">We''re glad you asked! Checkout our full list of features! <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>\r\n<div><em>All plans include a 14 day money back guarantee.</em></div>\r\n<div>Requires a current active license and subject to our <a href="https://ninjaforms.com/terms-conditions/?utm_medium=plugin&utm_source=plugin-dashboard&utm_campaign=Ninja+Forms+Memberships&utm_content=Terms+Conditions" target="_blank" rel="noopener">Terms &amp; Conditions</a>.</div>\r\n</div>', 'no');
INSERT INTO `lab_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(268, 'ninja_forms_addons_feed', '[{"title":"Conditional Logic","image":"assets\\/img\\/add-ons\\/conditional-logic.png","content":"Build smart, dynamic, interactive WordPress forms that tailor themselves to what a user needs as they fill out the form.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/conditional-logic\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Conditional+Logic","plugin":"ninja-forms-conditionals\\/conditionals.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/conditional-logic\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Conditional+Logic+Docs","version":"3.0.22","categories":[{"name":"Look &amp; Feel","slug":"look-feel"},{"name":"Actions","slug":"actions"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"},{"name":"Business","slug":"business"}]},{"title":"Multi-Part Forms","image":"assets\\/img\\/add-ons\\/multi-part-forms.png","content":"Easily break long forms into multiple pages for a huge user experience upgrade!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/multi-part-forms\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Multi-Part+Forms","plugin":"ninja-forms-multi-part\\/multi-part.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/multi-part-forms\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Multi-Part+Forms+Docs","version":"3.0.24","categories":[{"name":"Look &amp; Feel","slug":"look-feel"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"},{"name":"Business","slug":"business"}]},{"title":"Front-End Posting","image":"assets\\/img\\/add-ons\\/front-end-posting.png","content":"Allow users to create their own public posts and pages without ever seeing the Dashboard!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/post-creation\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Front-End+Posting","plugin":"ninja-forms-post-creation\\/ninja-forms-post-creation.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/post-creation\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Front-End+Posting+Docs","version":"3.0.7","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"}]},{"title":"File Uploads","image":"assets\\/img\\/add-ons\\/file-uploads.png","content":"Allow users to upload files and save them to your server, media library, or even Dropbox and Amazon S3!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/file-uploads\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=File+Uploads","plugin":"ninja-forms-uploads\\/file-uploads.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/file-uploads\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=File+Uploads+Docs","version":"3.0.22","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"},{"name":"Business","slug":"business"}]},{"title":"Layout and Styles","image":"assets\\/img\\/add-ons\\/layout-styles.png","content":"Easily create multi-column form layouts and beautifully styled WordPress forms without mastering CSS.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/layouts-and-styles\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Layout+and+Styles","plugin":"ninja-forms-style\\/ninja-forms-style.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/layouts-and-styles\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Layout+and+Styles+Docs","version":"3.0.25","categories":[{"name":"Look &amp; Feel","slug":"look-feel"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"},{"name":"Business","slug":"business"}]},{"title":"MailChimp","image":"assets\\/img\\/add-ons\\/mail-chimp.png","content":"Integrate MailChimp and WordPress with easy-to-create, fully customizable signup forms!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/mailchimp\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=MailChimp","plugin":"ninja-forms-mail-chimp\\/ninja-forms-mail-chimp.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/mailchimp\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=MailChimp+Docs","version":"3.1.9","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"},{"name":"Business","slug":"business"}]},{"title":"Campaign Monitor","image":"assets\\/img\\/add-ons\\/campaign-monitor.png","content":"Connect your website directly with Campaign Monitor using any of your WordPress forms!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/campaign-monitor\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Campaign+Monitor","plugin":"ninja-forms-campaign-monitor\\/ninja-forms-campaign-monitor.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/campaign-monitor\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Campaign+Monitor+Docs","version":"3.0.5","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Membership","slug":"membership"}]},{"title":"User Analytics","image":"assets\\/img\\/add-ons\\/user-analytics.png","content":"Collect valuable user data on form submission without the hassle of integrating with other services!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/user-analytics\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=User+Analytics","plugin":"ninja-forms-user-analytics\\/ninja-forms-user-analytics.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/user-analytics\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=User+Analytics+Docs","version":"3.0.0","categories":[{"name":"Notifications","slug":"notifications"},{"name":"Membership","slug":"membership"}]},{"title":"Constant Contact","image":"assets\\/img\\/add-ons\\/constant-contact.png","content":"Turn any WordPress form into a beautiful, fully customizable Constant Contact signup form in minutes!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/constant-contact\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Constant+Contact","plugin":"ninja-forms-constant-contact\\/ninja-forms-constant-contact.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/constant-contact\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Constant+Contact+Docs","version":"3.0.4","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Membership","slug":"membership"}]},{"title":"AWeber","image":"assets\\/img\\/add-ons\\/aweber.png","content":"Integrate AWeber and WordPress for fully automated email marketing in minutes!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/aweber\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=AWeber","plugin":"ninja-forms-aweber\\/ninja-forms-aweber.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/aweber\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=AWeber+Docs","version":"3.0.2","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Membership","slug":"membership"}]},{"title":"PayPal Express","image":"assets\\/img\\/add-ons\\/paypal-express.png","content":"Connect WordPress with PayPal Express & start collecting payments with your WordPress forms!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/paypal-express\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PayPal+Express","plugin":"ninja-forms-paypal-express\\/ninja-forms-paypal-express.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/paypal-express\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PayPal+Express+Docs","version":"3.0.14","categories":[{"name":"Payment Gateways","slug":"payment-gateways"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"},{"name":"Business","slug":"business"}]},{"title":"Zoho CRM","image":"assets\\/img\\/add-ons\\/zoho-crm.png","content":"Sell smarter, better, and faster with total integration between Zoho CRM and WordPress","link":"https:\\/\\/ninjaforms.com\\/extensions\\/zoho-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Zoho+CRM","plugin":"ninja-forms-zoho-crm\\/ninja-forms-zoho-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/zoho-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Zoho+CRM+Docs","version":"3.3.3","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"Capsule CRM","image":"assets\\/img\\/add-ons\\/capsule-crm.png","content":"Save time to focus on sales with direct, flawless integration with your Capsule CRM account","link":"https:\\/\\/ninjaforms.com\\/extensions\\/capsule-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Capsule+CRM","plugin":"ninja-forms-capsule-crm\\/ninja-forms-capsule-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/capsule-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Capsule+CRM+Docs","version":"3.3.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"Stripe","image":"assets\\/img\\/add-ons\\/stripe.png","content":"Accept credit cards payments securely and efficiently from your WordPress forms","link":"https:\\/\\/ninjaforms.com\\/extensions\\/stripe\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Stripe","plugin":"ninja-forms-stripe\\/ninja-forms-stripe.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/stripe\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Stripe+Docs","version":"3.0.19","categories":[{"name":"Payment Gateways","slug":"payment-gateways"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"}]},{"title":"Insightly CRM","image":"assets\\/img\\/add-ons\\/insightly-crm.png","content":"Connect your WordPress forms and Insightly CRM to build better customer relationships and accelerate sales","link":"https:\\/\\/ninjaforms.com\\/extensions\\/insightly-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Insightly+CRM","plugin":"ninja-forms-insightly-crm\\/ninja-forms-insightly-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/insightly-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Insightly+CRM+Docs","version":"3.2.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"PDF Form Submission","image":"assets\\/img\\/add-ons\\/pdf-form-submission.png","content":"Easily create standardized PDF copies of any form submission to export or email","link":"https:\\/\\/ninjaforms.com\\/extensions\\/pdf\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PDF+Form+Submission","plugin":"ninja-forms-pdf-submissions\\/nf-pdf-submissions.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/pdf\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PDF+Form+Submission+Docs","version":"3.0.5","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Membership","slug":"membership"},{"name":"Business","slug":"business"}]},{"title":"Sendy","image":"assets\\/img\\/add-ons\\/sendy.png","content":"Sendy extension for Ninja Forms lets you subscribe users using Ninja Forms.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/sendy\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Sendy","plugin":"ninja-forms-sendy\\/ninja-forms-sendy.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/sendy\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Sendy+Docs","version":"3.0","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Membership","slug":"membership"}]},{"title":"Trello","image":"assets\\/img\\/add-ons\\/trello.png","content":"Create Trello cards from your Ninja Forms submissions.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/trello\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Trello","plugin":"ninja-forms-trello\\/ninja-forms-trello.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/trello\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Trello+Docs","version":"3.0.2","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Membership","slug":"membership"}]},{"title":"Elavon","image":"assets\\/img\\/add-ons\\/elavon.png","content":"A simple, streamlined, and secure way to accept credit card payments in WordPress","link":"https:\\/\\/ninjaforms.com\\/extensions\\/elavon\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Elavon","plugin":"ninja-forms-elavon-payment-gateway\\/ninja-forms-elavon-payment-gateway.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/elavon\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Elavon+Docs","version":"3.0.1","categories":[{"name":"Payment Gateways","slug":"payment-gateways"},{"name":"Membership","slug":"membership"}]},{"title":"Zapier","image":"assets\\/img\\/add-ons\\/zapier.png","content":"Integrate WordPress with virtually any service in a matter of minutes!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/zapier\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Zapier","plugin":"ninja-forms-zapier\\/ninja-forms-zapier.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/zapier\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Zapier+Docs","version":"3.0.8","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Membership","slug":"membership"}]},{"title":"Salesforce CRM","image":"assets\\/img\\/add-ons\\/salesforce-crm.png","content":"Complete, effortless integration between Salesforce and WordPress","link":"https:\\/\\/ninjaforms.com\\/extensions\\/salesforce-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Salesforce+CRM","plugin":"ninja-forms-salesforce-crm\\/ninja-forms-salesforce-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/salesforce-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Salesforce+CRM+Docs","version":"3.2.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"Slack","image":"assets\\/img\\/add-ons\\/slack.png","content":"Stay in touch with real-time notifications every time a form is submitted. Get Slack for WordPress and never miss a thing!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/slack\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Slack","plugin":"ninja-forms-slack\\/ninja-forms-slack.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/slack\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Slack+Docs","version":"3.0.2","categories":[{"name":"Notifications","slug":"notifications"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"CleverReach","image":"assets\\/img\\/add-ons\\/cleverreach.png","content":"Connect to CleverReach and turn any of your WordPress forms into a CleverReach signup form in minutes!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/cleverreach\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=CleverReach","plugin":"ninja-forms-cleverreach\\/ninja-forms-cleverreach.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/cleverreach\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=CleverReach+Docs","version":"3.1.3","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Membership","slug":"membership"}]},{"title":"Webhooks","image":"assets\\/img\\/add-ons\\/webhooks.png","content":"Connect your WordPress forms to virtually any external service, no code required!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/webhooks\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Webhooks","plugin":"ninja-forms-webhooks\\/ninja-forms-webhooks.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/webhooks\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Webhooks+Docs","version":"3.0.5","categories":[{"name":"Notifications","slug":"notifications"},{"name":"Actions","slug":"actions"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"}]},{"title":"Excel Export","image":"assets\\/img\\/add-ons\\/excel-export.png","content":"Flawlessly export any number of form submissions into an Excel spreadsheet with the press of a button!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/excel-export\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Excel+Export","plugin":"ninja-forms-excel-export\\/ninja-forms-excel-export.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/excel-export\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Excel+Export+Docs","version":"3.3.1","categories":[{"name":"Membership","slug":"membership"}]},{"title":"WebMerge","image":"assets\\/img\\/add-ons\\/webmerge.png","content":"Populate documents in WordPress, digital or paper! Perfect for contracts, applications, government forms, patient registration, and more!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/webmerge\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=WebMerge","plugin":"ninja-forms-webmerge\\/ninja-forms-webmerge.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/webmerge\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=WebMerge+Docs","version":"3.0.3","categories":[{"name":"Actions","slug":"actions"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"}]},{"title":"Help Scout","image":"assets\\/img\\/add-ons\\/help-scout.png","content":"Bring excellent customer support home to your WordPress dashboard with Help Scout integration for Ninja Forms!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/help-scout\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Help+Scout","plugin":null,"docs":"https:\\/\\/ninjaforms.com\\/docs\\/help-scout\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Help+Scout+Docs","version":"3.0.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Emma","image":"assets\\/img\\/add-ons\\/emma.png","content":"Add users to Emma newsletters using Ninja Forms","link":"https:\\/\\/ninjaforms.com\\/extensions\\/emma\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Emma","plugin":"ninja-forms-emma\\/ninja-forms-emma.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/emma\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Emma+Docs","version":"3.0.4","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Actions","slug":"actions"},{"name":"Developer","slug":"developer"},{"name":"Membership","slug":"membership"},{"name":"User","slug":"user"}]},{"title":"ClickSend SMS","image":"assets\\/img\\/add-ons\\/clicksend-sms.png","content":"Send a SMS when someone submits your form via ClickSend.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/clicksend-sms\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=ClickSend+SMS","plugin":"ninja-forms-clicksend\\/ninja-forms-clicksend.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/clicksend-sms\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=ClickSend+SMS+Docs","version":"3.0.1","categories":[{"name":"Notifications","slug":"notifications"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Twilio SMS","image":"assets\\/img\\/add-ons\\/twilio-sms.png","content":"Send a SMS when someone submits your form via Twilio.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/twilio\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Twilio+SMS","plugin":"ninja-forms-twilio\\/ninja-forms-twilio.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/twilio\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Twilio+SMS+Docs","version":"3.0.1","categories":[{"name":"Notifications","slug":"notifications"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Recurly","image":"assets\\/img\\/add-ons\\/recurly.png","content":"Subscribe users to Recurly plans using Ninja Forms!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/recurly\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Recurly","plugin":"ninja-forms-recurly\\/ninja-forms-recurly.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/recurly\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Recurly+Docs","version":"3.0.4","categories":[{"name":"Payment Gateways","slug":"payment-gateways"},{"name":"Actions","slug":"actions"}]},{"title":"User Management","image":"assets\\/img\\/add-ons\\/user-management.png","content":"User registration and management made easy using your Wordpress forms. Simple. Beautiful. Efficient.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/user-management\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=User+Management","plugin":"ninja-forms-user-management\\/ninja-forms-user-management.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/user-management\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=User+Management+Docs","version":"3.0.11","categories":[{"name":"Content Management","slug":"content-management"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Save Progress","image":"assets\\/img\\/add-ons\\/save-progress.png","content":"Stop losing conversions on longer WordPress forms! Give your users a way to save their progress for any incomplete form.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/save-progress\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Save+Progress","plugin":"ninja-forms-save-progress\\/ninja-forms-save-progress.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/save-progress\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Save+Progress+Docs","version":"3.0.17","categories":[{"name":"Look &amp; Feel","slug":"look-feel"}]},{"title":"EmailOctopus","image":"assets\\/img\\/add-ons\\/emailoctopus.png","content":"Automation, integration, analytics... EmailOctopus is the email management solution that fills every need, and it''s now available for WordPress!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/emailoctopus\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=EmailOctopus","plugin":"ninja-forms-emailoctopus\\/ninja-forms-emailoctopus.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/emailoctopus\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=EmailOctopus+Docs","version":"3.0.0","categories":[{"name":"Email Marketing","slug":"email-marketing"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"},{"name":"Business","slug":"business"}]},{"title":"PipelineDeals CRM","image":"assets\\/img\\/add-ons\\/pipelinedeals-crm.png","content":"Automated, effortless integration with PipelineDeals CRM","link":"https:\\/\\/ninjaforms.com\\/extensions\\/pipelinedeals-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PipelineDeals+CRM","plugin":"ninja-forms-zoho-crm\\/zoho-integration.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/pipelinedeals-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=PipelineDeals+CRM+Docs","version":"3.0.1","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Highrise CRM","image":"assets\\/img\\/add-ons\\/highrise-crm.png","content":"Combine the functional simplicity of Highrise CRM + the power to build lasting customer relationships with your WordPress website.","link":"https:\\/\\/ninjaforms.com\\/extensions\\/highrise-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Highrise+CRM","plugin":"ninja-forms-highrise-crm\\/ninja-forms-highrise-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/highrise-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Highrise+CRM+Docs","version":"3.0.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"ConvertKit","image":"assets\\/img\\/add-ons\\/convertkit.png","content":"Integrate WordPress with ConvertKit seamlessly through your WordPress forms","link":"https:\\/\\/ninjaforms.com\\/extensions\\/convertkit\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=ConvertKit","plugin":"ninja-forms-convertkit\\/ninja-forms-convertkit.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/convertkit\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=ConvertKit+Docs","version":"3.0.2","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]},{"title":"IntelligenceWP","image":"assets\\/img\\/add-ons\\/intelligencewp.png","content":"Complete Google Analytics Integration for Your WordPress Forms","link":"https:\\/\\/ninjaforms.com\\/extensions\\/intelligencewp\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=IntelligenceWP","plugin":"ninja-forms-intelligencewp\\/ninja-forms-intelligencewp.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/intelligencewp\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=IntelligenceWP+Docs","version":"3.0.3","categories":[{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"OnePageCRM","image":"assets\\/img\\/add-ons\\/onepage-crm.png","content":"Integrate WordPress with OnePage CRM seamlessly through your WordPress forms","link":"https:\\/\\/ninjaforms.com\\/extensions\\/onepage-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=OnePageCRM","plugin":"ninja-forms-onepage-crm\\/ninja-forms-onepage-crm.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/onepage-crm\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=OnePageCRM+Docs","version":"3.0.0","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Actions","slug":"actions"},{"name":"Membership","slug":"membership"}]},{"title":"Active Campaign","image":"assets\\/img\\/add-ons\\/active-campaign.png","content":"Discover marketing automation that''s intelligent, powerful, and easy to use!","link":"https:\\/\\/ninjaforms.com\\/extensions\\/activecampaign\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Active+Campaign","plugin":"ninja-forms-active-campaign\\/ninja-forms-active-campaign.php","docs":"https:\\/\\/ninjaforms.com\\/docs\\/activecampaign\\/?utm_medium=plugin&utm_source=plugin-addons-page&utm_campaign=Ninja+Forms+Addons+Page&utm_content=Active+Campaign+Docs","version":"3.0.4","categories":[{"name":"CRM Integrations","slug":"crm-integrations"},{"name":"Membership","slug":"membership"}]}]', 'no'),
(438, 'wpmenucart', 'a:12:{s:10:"menu_slugs";a:1:{i:1;s:1:"0";}s:14:"always_display";s:0:"";s:12:"icon_display";s:1:"1";s:13:"items_display";s:1:"3";s:15:"items_alignment";s:8:"standard";s:12:"custom_class";s:0:"";s:14:"flyout_display";s:0:"";s:17:"flyout_itemnumber";s:1:"5";s:9:"cart_icon";s:1:"0";s:11:"shop_plugin";s:11:"woocommerce";s:12:"builtin_ajax";s:0:"";s:15:"hide_theme_cart";s:1:"1";}', 'yes'),
(442, 'wpo_wcpdf_settings_general', 'a:4:{s:16:"download_display";s:7:"display";s:13:"template_path";s:103:"/web/htdocs/www.mbsn.es/home/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/templates/Simple";s:13:"currency_font";s:0:"";s:10:"paper_size";s:2:"a4";}', 'yes'),
(443, 'wpo_wcpdf_documents_settings_invoice', 'a:1:{s:7:"enabled";i:1;}', 'yes'),
(444, 'wpo_wcpdf_documents_settings_packing-slip', 'a:1:{s:7:"enabled";i:1;}', 'yes'),
(445, 'wpo_wcpdf_settings_debug', 'a:2:{s:14:"enable_cleanup";i:1;s:12:"cleanup_days";i:7;}', 'yes'),
(448, 'wpo_wcpdf_version', '2.2.5', 'yes'),
(453, 'yit_recently_activated', 'a:1:{i:0;s:41:"yith-woocommerce-product-bundles/init.php";}', 'yes'),
(454, 'yit_plugin_fw_panel_wc_default_options_set', 'a:1:{s:15:"yith_wcpb_panel";b:1;}', 'yes'),
(455, '_site_transient_timeout_yith_promo_message', '3087972504', 'no'),
(456, '_site_transient_yith_promo_message', 'a:6:{s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:8:{s:4:"date";s:29:"Sun, 25 Nov 2018 10:08:24 GMT";s:12:"content-type";s:15:"application/xml";s:10:"set-cookie";s:137:"__cfduid=dc64fad3b05e72d8ef18e57538f2723941543140504; expires=Mon, 25-Nov-19 10:08:24 GMT; path=/; domain=.yithemes.com; HttpOnly; Secure";s:13:"last-modified";s:29:"Mon, 19 Nov 2018 11:09:41 GMT";s:9:"expect-ct";s:87:"max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"";s:6:"server";s:10:"cloudflare";s:6:"cf-ray";s:20:"47f35057fea0430a-MXP";s:16:"content-encoding";s:4:"gzip";}}s:4:"body";s:1566:"<?xml version="1.0" encoding="UTF-8"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2018-12-15</expiry_date>\n    <promo>\n        <promo_id>yithblackfridaydeal2018</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <banner>black.jpg</banner>\n        <description>30% discount on any of our products! Valid from 22nd November to 25th November.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n        </style>\n        <start_date>2018-11-22 00:00:00</start_date>\n        <end_date>2018-11-25 23:59:59</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithcybermonday2018</promo_id>\n        <title><![CDATA[<strong>YITH Cyber Monday</strong>]]></title>\n        <banner>cyber.jpg</banner>\n        <description>30% discount on any of our products! Valid only on November 26th.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#12fdd4</image_bg_color>\n        </style>\n        <start_date>2018-11-26 00:00:00</start_date>\n        <end_date>2018-11-27 07:00:00</end_date>\n    </promo>\n</promotions>";s:8:"response";a:2:{s:4:"code";i:200;s:7:"message";s:2:"OK";}s:7:"cookies";a:1:{i:0;O:14:"WP_Http_Cookie":5:{s:4:"name";s:8:"__cfduid";s:5:"value";s:43:"dc64fad3b05e72d8ef18e57538f2723941543140504";s:7:"expires";i:1574676504;s:4:"path";s:1:"/";s:6:"domain";s:12:"yithemes.com";}}s:8:"filename";N;s:13:"http_response";O:25:"WP_HTTP_Requests_Response":5:{s:11:"\0*\0response";O:17:"Requests_Response":10:{s:4:"body";s:1566:"<?xml version="1.0" encoding="UTF-8"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2018-12-15</expiry_date>\n    <promo>\n        <promo_id>yithblackfridaydeal2018</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <banner>black.jpg</banner>\n        <description>30% discount on any of our products! Valid from 22nd November to 25th November.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n        </style>\n        <start_date>2018-11-22 00:00:00</start_date>\n        <end_date>2018-11-25 23:59:59</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithcybermonday2018</promo_id>\n        <title><![CDATA[<strong>YITH Cyber Monday</strong>]]></title>\n        <banner>cyber.jpg</banner>\n        <description>30% discount on any of our products! Valid only on November 26th.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#12fdd4</image_bg_color>\n        </style>\n        <start_date>2018-11-26 00:00:00</start_date>\n        <end_date>2018-11-27 07:00:00</end_date>\n    </promo>\n</promotions>";s:3:"raw";s:2071:"HTTP/1.1 200 OK\r\nDate: Sun, 25 Nov 2018 10:08:24 GMT\r\nContent-Type: application/xml\r\nTransfer-Encoding: chunked\r\nConnection: close\r\nSet-Cookie: __cfduid=dc64fad3b05e72d8ef18e57538f2723941543140504; expires=Mon, 25-Nov-19 10:08:24 GMT; path=/; domain=.yithemes.com; HttpOnly; Secure\r\nLast-Modified: Mon, 19 Nov 2018 11:09:41 GMT\r\nExpect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"\r\nServer: cloudflare\r\nCF-RAY: 47f35057fea0430a-MXP\r\nContent-Encoding: gzip\r\n\r\n<?xml version="1.0" encoding="UTF-8"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2018-12-15</expiry_date>\n    <promo>\n        <promo_id>yithblackfridaydeal2018</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <banner>black.jpg</banner>\n        <description>30% discount on any of our products! Valid from 22nd November to 25th November.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n        </style>\n        <start_date>2018-11-22 00:00:00</start_date>\n        <end_date>2018-11-25 23:59:59</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithcybermonday2018</promo_id>\n        <title><![CDATA[<strong>YITH Cyber Monday</strong>]]></title>\n        <banner>cyber.jpg</banner>\n        <description>30% discount on any of our products! Valid only on November 26th.</description>\n        <link>\n            <label><![CDATA[Don''t miss it and <strong>INCREASE YOUR E-COMMERCE CONVERSIONS NOW!!!</strong>]]></label>\n            <url>https://yithemes.com/?refer_id=1072986</url>\n        </link>\n        <style>\n            <image_bg_color>#12fdd4</image_bg_color>\n        </style>\n        <start_date>2018-11-26 00:00:00</start_date>\n        <end_date>2018-11-27 07:00:00</end_date>\n    </promo>\n</promotions>";s:7:"headers";O:25:"Requests_Response_Headers":1:{s:7:"\0*\0data";a:8:{s:4:"date";a:1:{i:0;s:29:"Sun, 25 Nov 2018 10:08:24 GMT";}s:12:"content-type";a:1:{i:0;s:15:"application/xml";}s:10:"set-cookie";a:1:{i:0;s:137:"__cfduid=dc64fad3b05e72d8ef18e57538f2723941543140504; expires=Mon, 25-Nov-19 10:08:24 GMT; path=/; domain=.yithemes.com; HttpOnly; Secure";}s:13:"last-modified";a:1:{i:0;s:29:"Mon, 19 Nov 2018 11:09:41 GMT";}s:9:"expect-ct";a:1:{i:0;s:87:"max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"";}s:6:"server";a:1:{i:0;s:10:"cloudflare";}s:6:"cf-ray";a:1:{i:0;s:20:"47f35057fea0430a-MXP";}s:16:"content-encoding";a:1:{i:0;s:4:"gzip";}}}s:11:"status_code";i:200;s:16:"protocol_version";d:1.1000000000000001;s:7:"success";b:1;s:9:"redirects";i:0;s:3:"url";s:48:"https://update.yithemes.com/promo/yith-promo.xml";s:7:"history";a:0:{}s:7:"cookies";O:19:"Requests_Cookie_Jar":1:{s:10:"\0*\0cookies";a:1:{s:8:"__cfduid";O:15:"Requests_Cookie":5:{s:4:"name";s:8:"__cfduid";s:5:"value";s:43:"dc64fad3b05e72d8ef18e57538f2723941543140504";s:10:"attributes";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:5:{s:7:"expires";i:1574676504;s:4:"path";s:1:"/";s:6:"domain";s:12:"yithemes.com";s:8:"httponly";b:1;s:6:"secure";b:1;}}s:5:"flags";a:4:{s:8:"creation";i:1543140504;s:11:"last-access";i:1543140504;s:10:"persistent";b:0;s:9:"host-only";b:0;}s:14:"reference_time";i:1543140504;}}}}s:11:"\0*\0filename";N;s:4:"data";N;s:7:"headers";N;s:6:"status";N;}}', 'no'),
(329, 'woocommerce_tax_classes', 'Reduced rate\nZero rate', 'yes'),
(330, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(331, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(332, 'woocommerce_price_display_suffix', '', 'yes'),
(333, 'woocommerce_tax_total_display', 'itemized', 'no'),
(334, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(335, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(336, 'woocommerce_ship_to_destination', 'billing', 'no'),
(337, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(338, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(339, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(340, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(341, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(342, 'woocommerce_registration_generate_username', 'yes', 'no'),
(343, 'woocommerce_registration_generate_password', 'yes', 'no'),
(344, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(345, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(346, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(347, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(348, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(349, 'woocommerce_trash_pending_orders', '', 'no'),
(350, 'woocommerce_trash_failed_orders', '', 'no'),
(351, 'woocommerce_trash_cancelled_orders', '', 'no'),
(352, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(353, 'woocommerce_email_from_name', 'marcomasoero', 'no'),
(354, 'woocommerce_email_from_address', 'antonio@maisto.info', 'no'),
(355, 'woocommerce_email_header_image', '', 'no'),
(356, 'woocommerce_email_footer_text', '{site_title}<br/>Powered by <a href="https://woocommerce.com/">WooCommerce</a>', 'no'),
(357, 'woocommerce_email_base_color', '#96588a', 'no'),
(358, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(359, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(360, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(361, 'woocommerce_cart_page_id', '', 'yes'),
(362, 'woocommerce_checkout_page_id', '', 'yes'),
(363, 'woocommerce_myaccount_page_id', '', 'yes'),
(364, 'woocommerce_terms_page_id', '', 'no'),
(365, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(366, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(367, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(368, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(369, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(370, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(371, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(372, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(373, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(374, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(375, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(376, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(377, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(378, 'woocommerce_api_enabled', 'no', 'yes'),
(379, 'woocommerce_single_image_width', '600', 'yes'),
(380, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(381, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(382, 'woocommerce_demo_store', 'no', 'no'),
(383, 'woocommerce_permalinks', 'a:5:{s:12:"product_base";s:7:"product";s:13:"category_base";s:16:"product-category";s:8:"tag_base";s:11:"product-tag";s:14:"attribute_base";s:0:"";s:22:"use_verbose_page_rules";b:0;}', 'yes'),
(384, 'current_theme_supports_woocommerce', 'no', 'yes'),
(385, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(836, 'widget_monarchwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(386, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(387, 'product_cat_children', 'a:0:{}', 'yes'),
(388, 'default_product_cat', '15', 'yes'),
(563, '_site_transient_timeout_browser_45042d96ffcb6365f0c618982bb5a7ff', '1544384061', 'no'),
(564, '_site_transient_browser_45042d96ffcb6365f0c618982bb5a7ff', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"70.0.3538.102";s:8:"platform";s:9:"Macintosh";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(410, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(597, 'woocommerce_version', '3.5.2', 'yes'),
(943, 'woocommerce_db_version', '3.5.2', 'yes'),
(393, 'woocommerce_admin_notices', 'a:1:{i:0;s:6:"update";}', 'yes'),
(394, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(395, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(396, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(397, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(398, 'widget_woocommerce_price_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(399, 'widget_woocommerce_product_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(400, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(401, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(402, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(403, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(404, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(405, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(406, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(408, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:14:"total_comments";i:1;s:3:"all";i:1;s:8:"approved";s:1:"1";s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(409, '_transient_as_comment_count', 'O:8:"stdClass":7:{s:8:"approved";s:1:"1";s:14:"total_comments";i:1;s:3:"all";i:1;s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(421, '_transient_timeout_wc_low_stock_count', '1545732010', 'no'),
(422, '_transient_wc_low_stock_count', '0', 'no'),
(423, '_transient_timeout_wc_outofstock_count', '1545732010', 'no'),
(424, '_transient_wc_outofstock_count', '0', 'no'),
(855, 'opanda_default_social_locker_id', '28', 'yes'),
(776, '_site_transient_timeout_theme_roots', '1544030754', 'no'),
(777, '_site_transient_theme_roots', 'a:4:{s:7:"bizworx";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(646, 'duplicate_post_copytitle', '1', 'yes'),
(647, 'duplicate_post_copydate', '0', 'yes'),
(648, 'duplicate_post_copystatus', '0', 'yes'),
(649, 'duplicate_post_copyslug', '0', 'yes'),
(650, 'duplicate_post_copyexcerpt', '1', 'yes');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_postmeta`
--

DROP TABLE IF EXISTS `lab_postmeta`;
CREATE TABLE IF NOT EXISTS `lab_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=70 ;

--
-- Dump dei dati per la tabella `lab_postmeta`
--

INSERT INTO `lab_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(32, 20, '_wp_attachment_context', 'upgrader'),
(47, 27, 'opanda_header', 'Sign In To Unlock This Content'),
(31, 20, '_wp_attached_file', '2018/12/duplicate-post.3.2.2.zip'),
(46, 27, 'opanda_item', 'signin-locker'),
(45, 27, 'factory_shortcodes_assets', 'a:0:{}'),
(48, 27, 'opanda_message', 'Please sign in. It''s free. Just click one of the buttons below to get instant access.'),
(49, 27, 'opanda_style', 'great-attractor'),
(50, 27, 'opanda_catch_leads', '1'),
(51, 27, 'opanda_connect_buttons', 'facebook,twitter,google'),
(52, 27, 'opanda_facebook_actions', 'signup'),
(53, 27, 'opanda_twitter_actions', 'signup'),
(54, 27, 'opanda_google_actions', 'signup'),
(55, 27, 'opanda_linkedin_actions', 'signup'),
(56, 27, 'opanda_email_actions', 'signup'),
(57, 27, 'opanda_mobile', '1'),
(58, 27, 'opanda_highlight', '1'),
(59, 27, 'opanda_is_system', '1'),
(60, 27, 'opanda_is_default', '1'),
(61, 28, 'factory_shortcodes_assets', 'a:0:{}'),
(62, 28, 'opanda_item', 'social-locker'),
(63, 28, 'opanda_header', 'This content is locked'),
(64, 28, 'opanda_message', 'Please support us, use one of the buttons below to unlock the content.'),
(65, 28, 'opanda_style', 'flat'),
(66, 28, 'opanda_mobile', '1'),
(67, 28, 'opanda_highlight', '1'),
(68, 28, 'opanda_is_system', '1'),
(69, 28, 'opanda_is_default', '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_posts`
--

DROP TABLE IF EXISTS `lab_posts`;
CREATE TABLE IF NOT EXISTS `lab_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dump dei dati per la tabella `lab_posts`
--

INSERT INTO `lab_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-11-16 15:26:04', '2018-11-16 15:26:04', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-11-16 15:26:04', '2018-11-16 15:26:04', '', 0, 'https://www.mbsn.es/?p=1', 0, 'post', '', 1),
(2, 1, '2018-11-16 15:26:04', '2018-11-16 15:26:04', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="https://www.mbsn.es/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-11-16 15:26:04', '2018-11-16 15:26:04', '', 0, 'https://www.mbsn.es/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-11-16 15:26:04', '2018-11-16 15:26:04', '<h2>Who we are</h2><p>Our website address is: https://www.mbsn.es.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-11-16 15:26:04', '2018-11-16 15:26:04', '', 0, 'https://www.mbsn.es/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-11-25 09:19:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-11-25 09:19:12', '0000-00-00 00:00:00', '', 0, 'https://www.mbsn.es/?p=4', 0, 'post', '', 0),
(19, 2, '2018-11-25 16:01:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-11-25 16:01:54', '0000-00-00 00:00:00', '', 0, 'https://www.mbsn.es/?p=19', 0, 'post', '', 0),
(20, 1, '2018-12-02 20:40:25', '2018-12-02 20:40:25', 'https://www.mbsn.es/wp-content/uploads/2018/12/duplicate-post.3.2.2.zip', 'duplicate-post.3.2.2.zip', '', 'private', 'open', 'closed', '', 'duplicate-post-3-2-2-zip', '', '', '2018-12-02 20:40:25', '2018-12-02 20:40:25', '', 0, 'https://www.mbsn.es/wp-content/uploads/2018/12/duplicate-post.3.2.2.zip', 0, 'attachment', '', 0),
(27, 1, '2018-12-05 17:13:37', '2018-12-05 17:13:37', '', 'Sign-In Locker (default)', '', 'publish', 'closed', 'closed', '', 'opanda_default_signin_locker', '', '', '2018-12-05 17:13:37', '2018-12-05 17:13:37', '', 0, 'https://www.mbsn.es/2018/12/05/opanda_default_signin_locker/', 0, 'opanda-item', '', 0),
(28, 1, '2018-12-05 17:13:37', '2018-12-05 17:13:37', '', 'Social Locker (default)', '', 'publish', 'closed', 'closed', '', 'opanda_default_social_locker', '', '', '2018-12-05 17:13:37', '2018-12-05 17:13:37', '', 0, 'https://www.mbsn.es/2018/12/05/opanda_default_social_locker/', 0, 'opanda-item', '', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_termmeta`
--

DROP TABLE IF EXISTS `lab_termmeta`;
CREATE TABLE IF NOT EXISTS `lab_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_terms`
--

DROP TABLE IF EXISTS `lab_terms`;
CREATE TABLE IF NOT EXISTS `lab_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dump dei dati per la tabella `lab_terms`
--

INSERT INTO `lab_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_term_relationships`
--

DROP TABLE IF EXISTS `lab_term_relationships`;
CREATE TABLE IF NOT EXISTS `lab_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `lab_term_relationships`
--

INSERT INTO `lab_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_term_taxonomy`
--

DROP TABLE IF EXISTS `lab_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `lab_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dump dei dati per la tabella `lab_term_taxonomy`
--

INSERT INTO `lab_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 0),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_usermeta`
--

DROP TABLE IF EXISTS `lab_usermeta`;
CREATE TABLE IF NOT EXISTS `lab_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dump dei dati per la tabella `lab_usermeta`
--

INSERT INTO `lab_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'fluidstream'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'lab_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'lab_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'default_password_nag', ''),
(17, 1, 'session_tokens', 'a:1:{s:64:"f491ca8efd910e5e5c5afd2386be79e596d92dbe8801c9056ae5375dc8312650";a:4:{s:10:"expiration";i:1544201630;s:2:"ip";s:12:"82.50.89.176";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";s:5:"login";i:1544028830;}}'),
(18, 1, 'lab_dashboard_quick_press_last_post_id', '4'),
(19, 1, 'community-events-location', 'a:1:{s:2:"ip";s:10:"82.50.89.0";}'),
(20, 1, 'tgmpa_dismissed_notice_tgmpa', '1'),
(21, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:0:{}}'),
(22, 2, 'nickname', 'info@marcomasoero.com'),
(23, 2, 'first_name', 'Marco'),
(24, 2, 'last_name', 'Masoero'),
(25, 2, 'description', ''),
(26, 2, 'rich_editing', 'true'),
(27, 2, 'syntax_highlighting', 'true'),
(28, 2, 'comment_shortcuts', 'false'),
(29, 2, 'admin_color', 'fresh'),
(30, 2, 'use_ssl', '0'),
(31, 2, 'show_admin_bar_front', 'true'),
(32, 2, 'locale', ''),
(33, 2, 'lab_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(34, 2, 'lab_user_level', '10'),
(35, 2, 'dismissed_wp_pointers', 'wp496_privacy'),
(36, 3, 'nickname', 'ilariaberry@gmail.com'),
(37, 3, 'first_name', 'Ilaria'),
(38, 3, 'last_name', 'Berry'),
(39, 3, 'description', ''),
(40, 3, 'rich_editing', 'true'),
(41, 3, 'syntax_highlighting', 'true'),
(42, 3, 'comment_shortcuts', 'false'),
(43, 3, 'admin_color', 'fresh'),
(44, 3, 'use_ssl', '0'),
(45, 3, 'show_admin_bar_front', 'true'),
(46, 3, 'locale', ''),
(47, 3, 'lab_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(48, 3, 'lab_user_level', '10'),
(49, 3, 'dismissed_wp_pointers', 'wp496_privacy'),
(50, 4, 'nickname', 'c.appetito@gmail.com'),
(51, 4, 'first_name', 'Claudio'),
(52, 4, 'last_name', 'Appetito'),
(53, 4, 'description', ''),
(54, 4, 'rich_editing', 'true'),
(55, 4, 'syntax_highlighting', 'true'),
(56, 4, 'comment_shortcuts', 'false'),
(57, 4, 'admin_color', 'fresh'),
(58, 4, 'use_ssl', '0'),
(59, 4, 'show_admin_bar_front', 'true'),
(60, 4, 'locale', ''),
(61, 4, 'lab_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(62, 4, 'lab_user_level', '10'),
(63, 4, 'dismissed_wp_pointers', 'wp496_privacy'),
(64, 5, 'nickname', 'andrea.pratico@yahoo.it'),
(65, 5, 'first_name', 'Andrea'),
(66, 5, 'last_name', 'Praticò'),
(67, 5, 'description', ''),
(68, 5, 'rich_editing', 'true'),
(69, 5, 'syntax_highlighting', 'true'),
(70, 5, 'comment_shortcuts', 'false'),
(71, 5, 'admin_color', 'fresh'),
(72, 5, 'use_ssl', '0'),
(73, 5, 'show_admin_bar_front', 'true'),
(74, 5, 'locale', ''),
(75, 5, 'lab_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(76, 5, 'lab_user_level', '10'),
(77, 5, 'dismissed_wp_pointers', 'wp496_privacy'),
(78, 2, 'default_password_nag', ''),
(79, 2, 'session_tokens', 'a:1:{s:64:"4bc2894931ce460b14644b327f5053356ab150479007b8fee07c892b4c2d5f82";a:4:{s:10:"expiration";i:1543334512;s:2:"ip";s:13:"93.36.189.136";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";s:5:"login";i:1543161712;}}'),
(80, 2, 'wc_last_active', '1543104000'),
(83, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:0:{}}'),
(82, 2, 'lab_dashboard_quick_press_last_post_id', '19'),
(84, 2, 'community-events-location', 'a:1:{s:2:"ip";s:11:"93.36.189.0";}'),
(85, 1, 'wc_last_active', '1543968000');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_users`
--

DROP TABLE IF EXISTS `lab_users`;
CREATE TABLE IF NOT EXISTS `lab_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `lab_users`
--

INSERT INTO `lab_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'fluidstream', '$P$BrVDyMX5C4p2mjswq1TExsiuDIDtU3.', 'fluidstream', 'antonio@maisto.info', '', '2018-11-16 15:26:04', '', 0, 'fluidstream'),
(2, 'info@marcomasoero.com', '$P$BfugAQOtyxmJdFj4YZXK4esdgn1x4M1', 'infomarcomasoero-com', 'info@marcomasoero.com', 'http://www.marcomasoero.com', '2018-11-25 10:10:09', '', 0, 'Marco Masoero'),
(3, 'ilariaberry@gmail.com', '$P$B1c9xl3L6bSEpUJpLLM3EJxEIMOHYn0', 'ilariaberrygmail-com', 'ilariaberry@gmail.com', 'http://www.berry-athletics.com', '2018-11-25 10:12:08', '1543140728:$P$Bj/q5jqeGSukuGKLG.liybOFdrAknM1', 0, 'Ilaria Berry'),
(4, 'c.appetito@gmail.com', '$P$BDg4QzgBv.Z4BJij8S4nFTWV7LxTNw1', 'c-appetitogmail-com', 'c.appetito@gmail.com', '', '2018-11-25 10:13:06', '1543140786:$P$BKRD1NCYgffsEhoQpTXX4AIf1vphrY/', 0, 'Claudio Appetito'),
(5, 'andrea.pratico@yahoo.it', '$P$Bi9kAom1Z388m.ZurOhqdBGPF8hZaV/', 'andrea-praticoyahoo-it', 'andrea.pratico@yahoo.it', '', '2018-11-25 10:13:49', '1543140829:$P$BzuUJ7MhCFJ.32aOyGa6GxqgnjnLgA0', 0, 'Andrea Praticò');

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_wc_download_log`
--

DROP TABLE IF EXISTS `lab_wc_download_log`;
CREATE TABLE IF NOT EXISTS `lab_wc_download_log` (
  `download_log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  PRIMARY KEY (`download_log_id`),
  KEY `permission_id` (`permission_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_wc_webhooks`
--

DROP TABLE IF EXISTS `lab_wc_webhooks`;
CREATE TABLE IF NOT EXISTS `lab_wc_webhooks` (
  `webhook_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`webhook_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_api_keys`
--

DROP TABLE IF EXISTS `lab_woocommerce_api_keys`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_api_keys` (
  `key_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  PRIMARY KEY (`key_id`),
  KEY `consumer_key` (`consumer_key`),
  KEY `consumer_secret` (`consumer_secret`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_attribute_taxonomies`
--

DROP TABLE IF EXISTS `lab_woocommerce_attribute_taxonomies`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_downloadable_product_permissions`
--

DROP TABLE IF EXISTS `lab_woocommerce_downloadable_product_permissions`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_log`
--

DROP TABLE IF EXISTS `lab_woocommerce_log`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`log_id`),
  KEY `level` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_order_itemmeta`
--

DROP TABLE IF EXISTS `lab_woocommerce_order_itemmeta`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_order_items`
--

DROP TABLE IF EXISTS `lab_woocommerce_order_items`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_order_items` (
  `order_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_payment_tokenmeta`
--

DROP TABLE IF EXISTS `lab_woocommerce_payment_tokenmeta`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `payment_token_id` (`payment_token_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_payment_tokens`
--

DROP TABLE IF EXISTS `lab_woocommerce_payment_tokens`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_payment_tokens` (
  `token_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_sessions`
--

DROP TABLE IF EXISTS `lab_woocommerce_sessions`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_sessions` (
  `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_key` (`session_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `lab_woocommerce_sessions`
--

INSERT INTO `lab_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(4, '1', 'a:9:{s:4:"cart";s:6:"a:0:{}";s:11:"cart_totals";s:367:"a:15:{s:8:"subtotal";i:0;s:12:"subtotal_tax";i:0;s:14:"shipping_total";i:0;s:12:"shipping_tax";i:0;s:14:"shipping_taxes";a:0:{}s:14:"discount_total";i:0;s:12:"discount_tax";i:0;s:19:"cart_contents_total";i:0;s:17:"cart_contents_tax";i:0;s:19:"cart_contents_taxes";a:0:{}s:9:"fee_total";i:0;s:7:"fee_tax";i:0;s:9:"fee_taxes";a:0:{}s:5:"total";i:0;s:9:"total_tax";i:0;}";s:15:"applied_coupons";s:6:"a:0:{}";s:22:"coupon_discount_totals";s:6:"a:0:{}";s:26:"coupon_discount_tax_totals";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:8:"customer";s:707:"a:26:{s:2:"id";s:1:"1";s:13:"date_modified";s:0:"";s:8:"postcode";s:0:"";s:4:"city";s:0:"";s:9:"address_1";s:0:"";s:7:"address";s:0:"";s:9:"address_2";s:0:"";s:5:"state";s:0:"";s:7:"country";s:2:"IT";s:17:"shipping_postcode";s:0:"";s:13:"shipping_city";s:0:"";s:18:"shipping_address_1";s:0:"";s:16:"shipping_address";s:0:"";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:0:"";s:16:"shipping_country";s:2:"IT";s:13:"is_vat_exempt";s:0:"";s:19:"calculated_shipping";s:0:"";s:10:"first_name";s:0:"";s:9:"last_name";s:0:"";s:7:"company";s:0:"";s:5:"phone";s:0:"";s:5:"email";s:19:"antonio@maisto.info";s:19:"shipping_first_name";s:0:"";s:18:"shipping_last_name";s:0:"";s:16:"shipping_company";s:0:"";}";s:16:"group_order_data";s:0:"";s:19:"wc_free_select_gift";s:6:"a:0:{}";}', 1544202923);

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_shipping_zones`
--

DROP TABLE IF EXISTS `lab_woocommerce_shipping_zones`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_shipping_zones` (
  `zone_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_shipping_zone_locations`
--

DROP TABLE IF EXISTS `lab_woocommerce_shipping_zone_locations`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) unsigned NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_shipping_zone_methods`
--

DROP TABLE IF EXISTS `lab_woocommerce_shipping_zone_methods`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) unsigned NOT NULL,
  `instance_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) unsigned NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_tax_rates`
--

DROP TABLE IF EXISTS `lab_woocommerce_tax_rates`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) unsigned NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) unsigned NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`),
  KEY `tax_rate_state` (`tax_rate_state`(2)),
  KEY `tax_rate_class` (`tax_rate_class`(10)),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `lab_woocommerce_tax_rate_locations`
--

DROP TABLE IF EXISTS `lab_woocommerce_tax_rate_locations`;
CREATE TABLE IF NOT EXISTS `lab_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
