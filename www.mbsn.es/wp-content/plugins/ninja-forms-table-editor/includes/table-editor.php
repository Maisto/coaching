<?php
require_once dirname( __FILE__ ) . '/abstracts/addon.php';

/**
 * Class NF_Table_Editor
 */
final class NF_Table_Editor extends NF_PVW_Abstract_Addon {

	public $plugin_name = 'Table Editor';
	public $plugin_option_prefix = 'table_editor';
	protected $class_prefix = 'NF_TE';
	protected $textdomain = 'ninja-forms-table-editor';
	public $csv;

	/**
	 * Field type
	 */
	const TYPE = 'table_editor';

	/**
	 * @return bool
	 */
	protected function is_ninja_forms_three() {
		// Perform update if necessary for forms
		new NF_TE_Admin_Upgrade();

		return parent::is_ninja_forms_three();
	}

	/**
	 * Load the Table Editor plugin for deprecated Ninja Form installs
	 */
	protected function load_deprecated() {
		require_once dirname( $this->plugin_file_path ) . '/deprecated/deprecated-table-editor.php';
		if ( ! defined( 'NINJA_FORMS_TABLE_EDITOR_URL' ) ) {
			define( 'NINJA_FORMS_TABLE_EDITOR_URL', plugins_url() . "/" . basename( dirname( $this->plugin_file_path ) ) );
		}

		new ninja_forms_table_editor( $this->plugin_version );
	}

	/**
	 * Load all the 3.0+ plugin code
	 */
	public function load_plugin() {
		new NF_TE_Display_Render();
		$this->csv = new NF_TE_Admin_CSV();
	}

	protected function three_init() {
		add_filter( 'ninja_forms_register_fields', array( $this, 'register_field' ) );

		new NF_TE_Integrations_NinjaForms_MergeTags();
		new NF_TE_Integrations_NinjaForms_Submission();
		new NF_TE_Integrations_NinjaForms_Attachments();
		new NF_TE_Integrations_NinjaForms_Pdf();
	}

	public function register_field( $fields ) {
		$fields['table_editor'] = new NF_TE_Fields_Table();

		return $fields;
	}
}