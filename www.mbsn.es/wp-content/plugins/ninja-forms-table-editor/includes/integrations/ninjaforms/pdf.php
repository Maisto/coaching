<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Integrations_NinjaForms_Pdf {

	/**
	 * NF_TE_Integrations_NinjaForms_Pdf constructor.
	 */
	public function __construct() {
		add_filter( 'ninja_forms_pdf_pre_user_value', array( $this, 'pre_user_value' ) );
		add_filter( 'ninja_forms_table_editor_field_settings', array( $this, 'add_field_setting' ) );
		add_filter( 'ninja_forms_pdf_field_value', array( $this, 'ninja_forms_pdf_field_value' ), 10, 3 );
		add_filter( 'ninja_forms_pdf_field_value_wpautop', array( $this, 'ninja_forms_pdf_field_value_wpautop' ), 10, 3 );
	}

	/**
	 * Make sure the field value is a simple string to stop array conversion issues when exploding a multidimensional array.
	 *
	 * @param mixed $field_value
	 *
	 * @return mixed
	 */
	function pre_user_value( $field_value ) {
		if ( is_array( $field_value ) && isset( $field_value['csv_url'] ) ) {
			return $field_value['csv_url'] ;
		}

		return $field_value;
	}

	/**
	 * Add PDF display type to the TE field settings.
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function add_field_setting( $settings ) {
		$settings['pdf_format'] = array(
			'name'    => 'pdf_format',
			'type'    => 'select',
			'label'   => __( 'Table Format in PDF', 'ninja-forms-table-editor' ),
			'options' => array(
				array(
					'value' => 'slim',
					'label' => __( 'Slim', 'ninja-forms-table-editor' ),
				),
				array(
					'value' => 'html',
					'label' => __( 'HTML', 'ninja-forms-table-editor' ),
				),
			),
			'value'   => 'slim',
			'group'   => 'advanced',
			'width'   => 'full',
			'help'    => __( 'Configure the output format of the table in a PDF.', 'ninja-forms-table-editor' ),
		);

		return $settings;
	}

	/**
	 * @param string $decoded_field_value
	 * @param mixed  $field_value
	 * @param array  $field
	 *
	 * @return string
	 */
	public function ninja_forms_pdf_field_value( $decoded_field_value, $field_value, $field ) {
		if ( NF_Table_Editor::TYPE == $field['type'] ) {
			if ( isset( $field['pdf_format'] ) && 'html' === $field['pdf_format'] ) {
				return NF_TE_Admin_Handsontable::output_full( $field );
			}

			return NF_TE_Admin_Handsontable::output_slim( $field );

		}

		return $decoded_field_value;
	}

	/**
	 * Override the PDF submission using wpautop() on the submitted value
	 * for the Table Editor.
	 *
	 * @param bool  $wpautop
	 * @param array $field_value
	 * @param array $field
	 *
	 * @return bool
	 */
	public function ninja_forms_pdf_field_value_wpautop( $wpautop, $field_value, $field ) {
		if ( NF_Table_Editor::TYPE === $field['type'] ) {
			return false;
		}

		return $wpautop;
	}
}