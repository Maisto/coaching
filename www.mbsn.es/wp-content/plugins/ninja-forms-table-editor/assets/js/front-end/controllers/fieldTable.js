(function( $, nfte ) {

	var nfRadio = Backbone.Radio;
	var radioChannel = nfRadio.channel( 'table_editor' );
	var tableController = Marionette.Object.extend( {

		initialize: function() {
			this.listenTo( radioChannel, 'init:model', this.initConfig );
			this.listenTo( radioChannel, 'attach:view', this.initTable );
			radioChannel.reply( 'get:submitData', this.getSubmitData );
			radioChannel.reply( 'validate:required', this.validateRequired );
			radioChannel.reply( 'validate:modelData', this.validateModelRequired );
		},

		initConfig: function( model ) {
			model.set( 'wrapperClass', ('scroll' === model.get( 'table_width' ) ) ? 'table-scroll' : '' );
			var config = nfte.tableEditor.initConfig( model.get( 'tableConfig' ), model.get( 'id' ) );
			model.set( 'config', config );
		},

		initTable: function( view ) {
			var hot = nfte.tableEditor.initTable( view.model.id, view.model.get( 'config' ), view.model.get( 'table_width' ), view.model.get( 'number_columns' ), '.nf-field-element' );
			view.model.set( 'hot', hot );
			hot.model = view.model;
			hot.controller = this;

			Handsontable.hooks.add( 'afterChange', function() {
				this.controller.saveData( this.model );
			}, hot );
		},

		saveData: function( model ) {
			var table_data = nfte.tableEditor.getTableData( model.get( 'hot' ), model.get( 'number_columns' ), model.get( 'min_rows' ) );
			model.set( 'raw_data', table_data );

			// Trigger change of field for validation
			nfRadio.channel( 'fields' ).trigger( 'change:field', '', model );
		},

		getSubmitData: function( fieldData, field ) {
			fieldData.raw_data = field.get( 'raw_data' );

			return fieldData;
		},

		/**
		 * Check the table has data.
		 *
		 * @param el
		 * @param model
		 * @returns {boolean}
		 */
		validateRequired: function( el, model ) {
			return nfte.tableEditor.validateCallback( model );
		},

		/**
		 * Check the table has data.
		 *
		 * @param model
		 * @returns {boolean}
		 */
		validateModelRequired: function( model ) {
			return nfte.tableEditor.validateCallback( model );
		},

	} );

	new tableController();

})( jQuery, window.nfte );
