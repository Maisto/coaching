=== Ninja Forms: Table Editor ===
Contributors: polevaultweb
Author: polevaultweb
Author URI: http://www.polevaultweb.com
Plugin URI: http://www.polevaultweb.com
Tags: ninja forms, field, table, grid, html, editor, excel
Requires at least: 3.0
Tested up to: 4.9.5
Stable tag: 3.0.4
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

This extension gives you a new field to collect tabular data using an Excel like grid. Set the number of columns and the headings and other table behaviour. The data is then attached to the administrator email as a CSV and stored in your WordPress uploads folder. The table editor allows users to add, remove rows, drag and copy cells just like Excel.

This plugin requires the free plugin [Ninja Forms](wordpress.org/plugins/ninja-forms/)

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the `ninja-forms-table-editor` directory to your `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Visit the 'Forms' menu item in your admin sidebar
4. When you create a form, you can now add table editor fields on the field edit page.

== Use ==

For help and video tutorials, please visit the Ninja Forms website: [NinjaForms.com](http://ninjaforms.com)

== Changelog ==

= 3.0.4 =

* Bug Fix - Tables not appearing in PDFs when exporting submission to PDF using the PDF addon

= 3.0.3 =

* Bug Fix - Any other attachments not being attached to emails.

= 3.0.2 =

* Bug Fix - CSV URL and path incorrect if you didn't have File Uploads addon
* Bug Fix - Issue with viewing submission table on some installs

= 3.0.1 =

* Bug Fix - Fatal error on PHP 5.2 installs

= 3.0 =

* New - Compatibility with Ninja Forms THREE

= 1.3.9 =

* Bug Fix - Table overlapping subsequent tables on row addition

= 1.3.8 =

* Fixed - PDF Submission addon integration not working

= 1.3.7 =

* Fixed - Table not rendering if conditionally hidden using the Conditional Logic addon
* New - Table width option of scroll, columns fit headers and overflow page with scroll
* New - Handsontable updated to 0.16.1
* Improvement - Compatible with WordPress 4.3

= 1.3.6 =

* Fixed - Double quotes in cells not properly escaped
* New - Handsontable updated to 0.13.1

= 1.3.5 =

* New - CSV delimiter and enclosure now respecting the Ninja Forms filters

= 1.3.4 =

* New - Table output option of slim or HTML when outputting to a PDF with the PDF Submission addon

= 1.3.3 =

* New - Handsontable updated to 0.12.2
* Fixed - Wrapping cell content not pushing subsequent page content down

= 1.3.2 =

* Fixed - Table data not being saved since Ninja Forms 2.8.9

= 1.3.1 =

* Fixed - Compatibility with Multi Part addon
* Added - Default number of columns for field with filter: `nf_table_editor_default_columns`

= 1.3 =

* Ability to choose the location of the submitted CSV, either Media library or `ninja-forms` folder in wp-content/uploads
* Compatibility with PDF submissions addon to show the table of data in the PDF.

= 1.2.1 =

* Compatibility with Ninja Forms 2.8

= 1.2 =

* Compatibility with Ninja Forms 2.7
* Handsontable updated to 0.11.0

= 1.1.1 =

* Fix 'Download CSV' was not a link

= 1.1 =

* Fix - An issue with the table not showing after submit for some users

= 1.0 =

* Initial Release
