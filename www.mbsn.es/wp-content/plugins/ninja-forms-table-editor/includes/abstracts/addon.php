<?php

if ( class_exists( 'NF_PVW_Abstract_Addon' ) ) {
	return;
}

/**
 * Class NF_PVW_Abstract_Addon
 */
abstract class NF_PVW_Abstract_Addon {

	/**
	 * @var
	 */
	private static $instance = array();

	/**
	 * @var string
	 */
	public $plugin_file_path;

	/**
	 * @var
	 */
	public $plugin_name;

	/**
	 * @var
	 */
	public $plugin_option_prefix;

	/**
	 * @var
	 */
	public $plugin_version;

	/**
	 * @var
	 */
	protected $textdomain;

	/**
	 * @var string
	 */
	protected $class_prefix;

	/**
	 * Main Plugin Instance
	 *
	 * Insures that only one instance of a plugin class exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @param string $class
	 * @param string $plugin_file_path
	 * @param string $plugin_version
	 *
	 * @return NF_Table_Editor Instance
	 */
	public static function instance( $class, $plugin_file_path, $plugin_version ) {
		if ( ! isset( self::$instance[ $class ] ) || ! ( self::$instance[ $class ] instanceof $class ) ) {
			self::$instance[ $class ] = new $class();

			spl_autoload_register( array( self::$instance[ $class ], 'autoloader' ) );

			// Initialize the class
			self::$instance[ $class ]->init( $plugin_file_path, $plugin_version );
		}

		return self::$instance[ $class ];
	}

	/**
	 * Initialize the class.
	 *
	 * @param string $plugin_file_path
	 * @param string $plugin_version
	 */
	protected function init( $plugin_file_path, $plugin_version ) {
		$this->plugin_file_path = $plugin_file_path;
		$this->plugin_version   = $plugin_version;

		add_action( 'admin_init', array( $this, 'setup_license' ) );

		if ( ! $this->is_ninja_forms_three() ) {
			$this->load_deprecated();

			return;
		}

		// This is THREE!
		add_filter( 'ninja_forms_field_template_file_paths', array( $this, 'register_template_path' ) );
		add_action( 'ninja_forms_loaded', array( $this, 'load_plugin' ) );
		add_action( 'init', array( $this, 'load_translations' ) );
		$this->three_init();
	}

	/**
	 * Load all the 3.0+ plugin code once Ninja Forms is loaded.
	 */
	public abstract function load_plugin();

	/**
	 * Load all the 3.0+ plugin code.
	 */
	protected abstract function three_init();

	/**
	 * Check the site is running Ninja Forms THREE.
	 *
	 * @return bool
	 */
	protected function is_ninja_forms_three() {
		if ( get_option( 'ninja_forms_load_deprecated', false ) ) {
			return false;
		}

		return version_compare( get_option( 'ninja_forms_version', '0' ), '3.0', '>=' );
	}

	/**
	 * Load the deprecated plugin files.
	 */
	protected abstract function load_deprecated();

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * class via the `new` operator from outside of this class.
	 */
	protected function __construct() {
	}

	/**
	 * As this class is a singleton it should not be clone-able
	 */
	protected function __clone() {
	}

	/**
	 * As this class is a singleton it should not be able to be unserialized
	 */
	protected function __wakeup() {
	}

	/**
	 * Autoload the classes
	 *
	 * @param string $class_name
	 */
	public function autoloader( $class_name ) {
		if ( class_exists( $class_name ) ) {
			return;
		}

		$classes_dir = realpath( plugin_dir_path( $this->plugin_file_path ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;

		$this->maybe_load_class( $class_name, $this->class_prefix, $classes_dir );
	}

	/**
	 * Load class file
	 *
	 * @param string $class
	 * @param string $prefix
	 * @param string $dir
	 * @param bool   $preserve_case
	 */
	public function maybe_load_class( $class, $prefix, $dir, $preserve_case = false ) {
		if ( false === strpos( $class, $prefix ) ) {
			return;
		}

		$class_name = str_replace( $prefix, '', $class );
		$class_name = $preserve_case ? $class_name : strtolower( $class_name );
		$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';

		if ( file_exists( $dir . $class_file ) ) {
			require_once $dir . $class_file;
		}
	}

	/**
	 * Licensing for the addon
	 */
	public function setup_license() {
		if ( ! class_exists( 'NF_Extension_Updater' ) ) {
			return;
		}

		new NF_Extension_Updater( $this->plugin_name, $this->plugin_version, 'polevaultweb', $this->plugin_file_path, $this->plugin_option_prefix );
	}

	/**
	 * Config
	 *
	 * @param string $file_name
	 * @param array  $data
	 *
	 * @return mixed
	 */
	public function config( $file_name, $data = array() ) {
		extract( $data );

		return include dirname( $this->plugin_file_path ) . '/includes/config/' . $file_name . '.php';
	}

	/**
	 * Template
	 *
	 * @param string $file_name
	 * @param array  $data
	 *
	 * @return mixed
	 */
	public function template( $file_name, array $data = array() ) {
		extract( $data );

		return include dirname( $this->plugin_file_path ) . '/includes/templates/' . $file_name . '.php';
	}

	/**
	 * Load translations for add-on.
	 * First, look in WP_LANG_DIR subfolder, then fallback to add-on plugin folder.
	 */
	public function load_translations() {
		$locale  = apply_filters( 'plugin_locale', get_locale(), $this->textdomain );
		$mo_file = $this->textdomain . '-' . $locale . '.mo';

		$wp_lang_dir = trailingslashit( WP_LANG_DIR ) . trailingslashit( dirname( $this->plugin_file_path ) );

		load_textdomain( $this->textdomain, $wp_lang_dir . $mo_file );

		$plugin_dir = trailingslashit( basename( dirname( $this->plugin_file_path ) ) );
		$lang_dir   = apply_filters( 'ninja_forms_' . $this->plugin_option_prefix . '_lang_dir', $plugin_dir . 'languages/' );
		load_plugin_textdomain( $this->textdomain, false, $lang_dir );
	}

	/**
	 * Register the template path for the plugin
	 *
	 * @param array $file_paths
	 *
	 * @return array
	 */
	public function register_template_path( $file_paths ) {
		$file_paths[] = dirname( $this->plugin_file_path ) . '/includes/templates/';

		return $file_paths;
	}

}