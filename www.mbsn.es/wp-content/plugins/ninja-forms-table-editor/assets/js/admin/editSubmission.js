(function( $, nfte ) {

	$( document ).ready( function() {
		// Initialize tables
		$.each( nf_table_editor.settings, function( field_id, settings ) {
			config = nfte.tableEditor.initConfig( settings.config, field_id );
			var hot = nfte.tableEditor.initTable( field_id, config, settings.table_width, settings.number_columns, '#nf-sub-fields' );
			nf_table_editor.settings[ field_id ].hot = hot;
		} );

		$( 'body' ).on( 'click', '.nf-sub-actions-save', function( event ) {
			// Save table updates when Submission is saved
			$.each( nf_table_editor.settings, function( field_id, settings ) {
				var old_data = JSON.stringify( settings.raw_data );
				var new_data = nfte.tableEditor.getTableData( settings.hot, settings.number_columns, settings.min_rows );

				if ( new_data != old_data ) {
					// Save data
					var sub_id = $('#post_ID').val();
					nf_te_update( sub_id, field_id, new_data );
				}
			} );
		} );

		function nf_te_update( sub_id, field_id, table_data ) {
			$.ajax( {
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'nf_te_sub_update_value',
					sub_id: sub_id,
					field_id: field_id,
					table_data: table_data
				},
				success: function( response ) {
				},
				error: function( response, status, error ) {
					alert( 'Error: ' + error.replace( /(<([^>]+)>)/ig, "" ) );
				}
			} );
		}
	} );

})( jQuery, window.nfte );