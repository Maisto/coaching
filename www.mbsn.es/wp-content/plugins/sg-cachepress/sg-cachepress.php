<?php
/**
 * SG CachePress
 *
 * @package           SG_CachePress
 * @author            SiteGround
 * @link              http://www.siteground.com/
 *
 * @wordpress-plugin
 * Plugin Name:       SG Optimizer
 * Description:       This plugin will link your WordPress application with all the performance optimizations provided by SiteGround
 * Version:           5.0.5
 * Author:            SiteGround
 * Text Domain:       sg-cachepress
 * Domain Path:       /languages
 */

// Our namespace.
namespace SiteGround_Optimizer;

use SiteGround_Optimizer\Helper\Helper;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Define version constant.
if ( ! defined( __NAMESPACE__ . '\VERSION' ) ) {
	define( __NAMESPACE__ . '\VERSION', '1.0.0' );
}

// Define slug constant.
if ( ! defined( __NAMESPACE__ . '\PLUGIN_SLUG' ) ) {
	define( __NAMESPACE__ . '\PLUGIN_SLUG', 'sg-cachepress' );
}

// Define root directory.
if ( ! defined( __NAMESPACE__ . '\DIR' ) ) {
	define( __NAMESPACE__ . '\DIR', __DIR__ );
}

// Define root URL.
if ( ! defined( __NAMESPACE__ . '\URL' ) ) {
	$url = \trailingslashit( DIR );

	// Sanitize directory separator on Windows.
	$url = str_replace( '\\', '/', $url );

	$wp_plugin_dir = str_replace( '\\', '/', WP_PLUGIN_DIR );
	$url = str_replace( $wp_plugin_dir, \plugins_url(), $url );

	define( __NAMESPACE__ . '\URL', \untrailingslashit( $url ) );
}

require_once( \SiteGround_Optimizer\DIR . '/vendor/autoload.php' );

// Initialize helper.
global $siteground_optimizer_helper;

if ( ! isset( $siteground_optimizer_helper ) ) {
	$siteground_optimizer_helper = new Helper();
}

