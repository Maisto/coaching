/*
 *  Edit Field
 *
 *  @description:
 *  @since: 1.0
 *  @created: 17/01/13
 */

jQuery(document).ready(function($) {


    /*----------------------------------------------------------------------
     *
     *	Headers Type
     *
     *---------------------------------------------------------------------*/

    $(document).on('change', 'select.ninja-forms-_table_editor-custom_headers', function( e ){
        e.preventDefault();
        toggle_headers(this);
    });

    $( 'select.ninja-forms-_table_editor-custom_headers' ).each(function() {
        toggle_headers(this);
    });

    function toggle_headers(element) {
        var val = $(element).val(),
            $tr = $(element).parents('span.field-option').parent().next();
        if( val == "custom" ) {
            $tr.show();
        }
        else {
            $tr.hide();
        }
    }


});