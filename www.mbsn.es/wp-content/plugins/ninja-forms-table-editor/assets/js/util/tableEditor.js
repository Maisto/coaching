/* global _wpUtilSettings */
window.nfte = window.nfte || {};

(function( $ ) {

	nfte.tableEditor = {
		initConfig: function( config, id ) {
			config[ 'afterRender' ] = function() {
				// refresh wrapper height
				$( '#table_wrapper_' + id ).height( $( "#table_" + id + ' .wtHider' ).height() );
			};

			return config;
		},

		initTable: function( id, config, table_width, number_columns, parent_element ) {
			var parent_width = $( '#table_wrapper_' + id ).parents( parent_element ).width();
			if ( config[ 'rowHeaders' ] ) {
				parent_width = parent_width - 50;
			}

			if ( table_width == 'stretch' ) {
				var col_width = parent_width / number_columns - 5;
				config[ 'colWidths' ] = Math.floor( col_width );
			}

			var container = document.getElementById( 'table_' + id );
			var hot = new Handsontable( container, config );

			return hot;
		},

		validateCallback: function( model ) {
			var cols = model.get( 'number_columns' );
			var rows = model.get( 'min_rows' );
			rows = rows == 0 ? 1 : rows;
			var emptyData = nfte.tableEditor.getEmptyData( cols, rows );
			emptyData = JSON.stringify( emptyData );
			var hot = model.get( 'hot' );
			var tableData = nfte.tableEditor.getTableData( hot, cols, rows );
			if ( tableData == emptyData ) {
				return false;
			}

			return true;
		},
		escapeQuotes: function( string ) {
			return string.replace( /"/g, '\\"' );
		},
		unescapeQuotes: function( string ) {
			return string.replace( /\\"/g, '"' );
		},

		getEmptyData: function( cols, rows ) {
			var row_data = [];
			var row_null_data = [];
			for ( var i = 0; i < cols; i++ ) {
				row_data.push( "" );
				row_null_data.push( null );
			}

			var table_data = [];
			for ( var i = 0; i < rows; i++ ) {
				if ( i > 0 ) {
					table_data.push( row_null_data );
				} else {
					table_data.push( row_data );
				}
			}

			return table_data;
		},

		getTableData: function( hot, number_columns, min_rows ) {
			var table_data = hot.getData();

			if ( typeof table_data === 'undefined' ) {
				min_rows = min_rows == 0 ? 1 : min_rows;

				return this.getEmptyData( number_columns, min_rows );
			}

			table_data = this.cleanTableData( table_data, true );
			table_data = JSON.stringify( table_data );

			return table_data;
		},

		cleanTableData: function( table_data, escape ) {
			var clean_table_data = [];
			if ( !table_data.length ) {
				return clean_table_data;
			}
			for ( var i = 0; i < table_data.length; i++ ) {
				var row = []
				for ( var j = 0; j < table_data[ i ].length; j++ ) {
					var cell = table_data[ i ][ j ];
					if ( cell !== null ) {
						if ( escape ) {
							cell = this.escapeQuotes( cell );
						} else {
							cell = this.unescapeQuotes( cell );
						}
					}
					row.push( cell );
				}
				clean_table_data.push( row );
			}

			return clean_table_data
		}
	};

})( jQuery );