<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Integrations_NinjaForms_Attachments {

	const KEY = 'field_list_te_email_attachments';

	/**
	 * NF_TE_Integrations_NinjaForms_Attachments constructor.
	 */
	public function __construct() {
		$actions_with_attachments = apply_filters( 'ninja_forms_table_editor_actions_with_attachments', array( 'email' ) );

		foreach ( $actions_with_attachments as $action ) {
			add_filter( 'ninja_forms_action_' . $action . '_settings', array( $this, 'email_settings' ) );
			add_filter( 'ninja_forms_action_' . $action . '_attachments', array( $this, 'attach_files' ), 10, 3 );
		}
	}

	/**
	 * Allow uploads to be attached to email actions
	 *
	 * @param array $settings
	 *
	 * @return array $settings
	 */
	public function email_settings( $settings ) {
		$form_id = filter_input( INPUT_GET, 'form_id', FILTER_VALIDATE_INT );

		if ( ! isset( $form_id ) ) {
			return $settings;
		}

		if ( ! $this->form_has_fields( $form_id ) ) {
			return $settings;
		}

		$settings[ self::KEY ] = array(
			'name'        => self::KEY,
			'type'        => 'field-list',
			'label'       => __( 'Attach Table CSVs', 'ninja-forms-table-editor' ),
			'width'       => 'full',
			'group'       => 'advanced',
			'field_types' => array( NF_Table_Editor::TYPE ),
			'settings'    => array(
				array(
					'name'  => 'toggle',
					'type'  => 'toggle',
					'label' => __( 'Field', 'ninja-forms-table-editor' ),
					'width' => 'full',
				),
			),
		);

		return $settings;
	}

	/**
	 * Has the form got our fields
	 *
	 * @param $form_id
	 *
	 * @return bool
	 */
	protected function form_has_fields( $form_id ) {
		foreach ( Ninja_Forms()->form( $form_id )->get_fields() as $field_id => $field ) {
			$get_settings = $field->get_settings();
			if ( NF_Table_Editor::TYPE == $get_settings['type'] ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Attach file to the email
	 *
	 * @param array $attachments
	 * @param array $data
	 * @param array $settings
	 *
	 * @return array
	 */
	public function attach_files( $attachments, $data, $settings ) {
		foreach ( $settings as $key => $value ) {
			if ( false === strpos( $key, self::KEY . '-' ) || 1 != $value ) {
				continue;
			}

			if ( ! isset( $data['fields'] ) ) {
				continue;
			}

			$field_key = str_replace( self::KEY . '-', '', $key );
			foreach ( $data['fields'] as $field ) {
				if ( $field_key != $field['key'] ) {
					continue;
				}

				if ( ! isset( $field['value']['csv_path'] ) ) {
					continue;
				}

				$attachments[] = $field['value']['csv_path'];
			}
		}

		return $attachments;
	}
}